/*
SQLyog Ultimate v11.42 (64 bit)
MySQL - 5.5.27 : Database - accequip
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`accequip` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `accequip`;

/*Table structure for table `department` */

DROP TABLE IF EXISTS `department`;

CREATE TABLE `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID департамента/отдела',
  `department_name` varchar(255) NOT NULL COMMENT 'Наименование департамента/отдела',
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `department` */

insert  into `department`(`department_id`,`department_name`) values (1,'IT-отдел'),(2,'Бухгалтерский отдел'),(3,'Финансовый отдел'),(4,'Административный отдел'),(5,'11111'),(6,'12345678');

/*Table structure for table `eqmove` */

DROP TABLE IF EXISTS `eqmove`;

CREATE TABLE `eqmove` (
  `eqmove_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID перемещения оборудования/ПО',
  `equipment_id` int(11) NOT NULL COMMENT 'ID оборудования',
  `eqmove_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата перемещения',
  `eqmove_officeold` int(11) NOT NULL COMMENT 'Откуда - отделение',
  `eqmove_departmentold` int(11) NOT NULL COMMENT 'Откуда - Департамент/отдел',
  `eqmove_officenew` int(11) NOT NULL COMMENT 'Куда - отделение',
  `eqmove_departmentnew` int(11) NOT NULL COMMENT 'Куда - Департамент/отдел',
  `equipment_invnom` varchar(50) DEFAULT NULL,
  `new_user_id` int(11) DEFAULT NULL,
  `old_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`eqmove_id`),
  KEY `equipment_id` (`equipment_id`),
  KEY `eqmove_officeold` (`eqmove_officeold`),
  KEY `eqmove_officenew` (`eqmove_officenew`),
  KEY `eqmove_departmentold` (`eqmove_departmentold`),
  KEY `eqmove_departmentnew` (`eqmove_departmentnew`),
  CONSTRAINT `eqmove_ibfk_1` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`equipment_id`),
  CONSTRAINT `eqmove_ibfk_2` FOREIGN KEY (`eqmove_officeold`) REFERENCES `office` (`office_id`),
  CONSTRAINT `eqmove_ibfk_3` FOREIGN KEY (`eqmove_officenew`) REFERENCES `office` (`office_id`),
  CONSTRAINT `eqmove_ibfk_4` FOREIGN KEY (`eqmove_departmentold`) REFERENCES `department` (`department_id`),
  CONSTRAINT `eqmove_ibfk_5` FOREIGN KEY (`eqmove_departmentnew`) REFERENCES `department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `eqmove` */

insert  into `eqmove`(`eqmove_id`,`equipment_id`,`eqmove_date`,`eqmove_officeold`,`eqmove_departmentold`,`eqmove_officenew`,`eqmove_departmentnew`,`equipment_invnom`,`new_user_id`,`old_user_id`) values (1,21,'2014-04-01 10:03:52',8,1,9,2,NULL,NULL,NULL),(2,26,'2014-04-16 13:56:21',9,3,8,1,NULL,NULL,NULL),(3,1,'2014-06-18 21:35:01',9,2,11,5,'00000001',4,7),(4,21,'2014-06-18 21:38:31',9,1,6,4,'00000004',9,4),(5,26,'2014-06-18 21:41:49',9,3,6,2,'00000006',0,9);

/*Table structure for table `eqremote` */

DROP TABLE IF EXISTS `eqremote`;

CREATE TABLE `eqremote` (
  `eqremote_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID списания',
  `equipment_id` int(11) NOT NULL COMMENT 'ID списанного оборудования',
  `eqremote_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата списания',
  PRIMARY KEY (`eqremote_id`),
  KEY `equipment_id` (`equipment_id`),
  CONSTRAINT `eqremote_ibfk_1` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`equipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `eqremote` */

insert  into `eqremote`(`eqremote_id`,`equipment_id`,`eqremote_date`) values (1,24,'2014-05-05 00:00:00'),(2,26,'2014-05-01 00:00:00'),(6,21,'2014-06-16 23:33:42'),(7,18,'2014-06-16 23:59:17');

/*Table structure for table `eqtype` */

DROP TABLE IF EXISTS `eqtype`;

CREATE TABLE `eqtype` (
  `eqtype_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID типа оборудования',
  `eqtype_name` varchar(50) NOT NULL COMMENT 'Наименование типа оборудования',
  PRIMARY KEY (`eqtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `eqtype` */

insert  into `eqtype`(`eqtype_id`,`eqtype_name`) values (1,'Монитор'),(2,'Системный блок'),(3,'Сканер'),(4,'Принтер'),(5,'МФУ'),(6,'Процессор'),(7,'Ноутбук'),(8,'fsdfvfd'),(9,'sdfghjk');

/*Table structure for table `equipment` */

DROP TABLE IF EXISTS `equipment`;

CREATE TABLE `equipment` (
  `equipment_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID оборудования/ПО',
  `equipment_name` varchar(255) NOT NULL COMMENT 'Наименование оборудования',
  `equipment_invnom` varchar(50) DEFAULT NULL COMMENT 'Инв. номер оборудования',
  `eqtype_id` int(11) NOT NULL COMMENT 'Тип оборудования',
  `equipment_characteristics` varchar(255) NOT NULL COMMENT 'Характеристики оборудования',
  `equipment_crtdate` date NOT NULL COMMENT 'Дата поступления оборудования',
  `equipment_warrdate` date NOT NULL COMMENT 'Дата окончания гарантийного периода',
  `equipment_life` date NOT NULL COMMENT 'Дата окончания срока службы',
  `office_id` int(11) NOT NULL COMMENT 'ID отделения',
  `department_id` int(11) NOT NULL COMMENT 'ID департамента/отдела',
  `user_id` int(11) DEFAULT NULL COMMENT 'ID пользователя',
  `provider_id` int(11) NOT NULL COMMENT 'ID поставщика',
  `equipment_cost` float(20,2) DEFAULT NULL COMMENT 'Стоимость оборудования',
  `equipment_flagremote` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-списано',
  `equipment_flagrepair` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-в ремонте',
  PRIMARY KEY (`equipment_id`),
  KEY `eqtype_id` (`eqtype_id`),
  KEY `office_id` (`office_id`),
  KEY `user_id` (`user_id`),
  KEY `provider_id` (`provider_id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `equipment_ibfk_1` FOREIGN KEY (`eqtype_id`) REFERENCES `eqtype` (`eqtype_id`),
  CONSTRAINT `equipment_ibfk_2` FOREIGN KEY (`office_id`) REFERENCES `office` (`office_id`),
  CONSTRAINT `equipment_ibfk_4` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`provider_id`),
  CONSTRAINT `equipment_ibfk_5` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Data for the table `equipment` */

insert  into `equipment`(`equipment_id`,`equipment_name`,`equipment_invnom`,`eqtype_id`,`equipment_characteristics`,`equipment_crtdate`,`equipment_warrdate`,`equipment_life`,`office_id`,`department_id`,`user_id`,`provider_id`,`equipment_cost`,`equipment_flagremote`,`equipment_flagrepair`) values (1,'Монитор 19\" Asus VS197DE','00000001',1,'Black / LED TN HD 1366x768 (16:9) 5мс / 50млн:1, 250 кд/м2, 170°/160° / VGA','2012-12-01','2015-12-01','2018-12-01',9,2,7,2,NULL,0,0),(16,'Монитор 19\" Philips 193V5LSB2/10/62','00000002',1,'Black / LED TN HD 1366x768 (16:9) 5мс / 700:1, 200 кд/м2, 90°/65° / VGA','2011-01-29','2014-07-01','2017-07-01',7,3,6,3,0.99,0,1),(18,'Монитор 24\" LG 24EA53VQ-P','00000003',1,'Black / LED IPS FullHD 1920x1080 (16:9) 5мс / 5млн:1, 250 кд/м2, 178°/178° / DVI, VGA, HDMI','2013-10-01','2016-10-01','2019-10-01',8,2,NULL,1,0.99,1,0),(21,'Ноутбук 15\" Asus X550CA-XX710D','00000004',7,'Black 15,6\" глянцевый LED HD (1366x768) / Intel Celeron 1007U 1,5GHz / DDR3 2Gb / HDD 320Gb 5400RPM / Intel HD2500 / DVD SM / DOS','2014-02-01','2016-02-01','2019-02-01',9,1,4,2,NULL,1,0),(24,'Сканер Epson Perfection V37 (B11B207303) Black','00000005',3,'Black / планшетный / CCD / USB 2.0 / 4800x9600 dpi / 48 bit','2012-06-01','2014-06-01','2017-06-01',9,1,4,3,0.99,1,0),(26,'МФУ струйное цветное Canon MG2440','00000006',5,'Gray / USB 2.0 / печать 8 стр мин 4800x600 dpi / скан 1200x600 dpi + СНПЧ WWM','2014-01-01','2016-01-01','2019-01-01',9,3,9,1,NULL,1,0);

/*Table structure for table `material` */

DROP TABLE IF EXISTS `material`;

CREATE TABLE `material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID расходного материала',
  `material_name` varchar(255) NOT NULL COMMENT 'Наименование материала',
  `mtype_id` int(11) NOT NULL COMMENT 'Тип материала',
  `material_count` int(11) NOT NULL COMMENT 'Количество материала',
  `office_id` int(11) NOT NULL COMMENT 'ID отделения',
  `department_id` int(11) NOT NULL COMMENT 'ID департамента/отдела',
  `provider_id` int(11) NOT NULL COMMENT 'ID поставщика',
  PRIMARY KEY (`material_id`),
  KEY `mtype_id` (`mtype_id`),
  KEY `office_id` (`office_id`),
  KEY `provider_id` (`provider_id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `material_ibfk_1` FOREIGN KEY (`mtype_id`) REFERENCES `mtype` (`mtype_id`),
  CONSTRAINT `material_ibfk_2` FOREIGN KEY (`office_id`) REFERENCES `office` (`office_id`),
  CONSTRAINT `material_ibfk_3` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`provider_id`),
  CONSTRAINT `material_ibfk_4` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `material` */

insert  into `material`(`material_id`,`material_name`,`mtype_id`,`material_count`,`office_id`,`department_id`,`provider_id`) values (1,'Картридж HP No.21 (C9351AE)',1,20,9,2,2),(3,'Картридж HP No.21 (C9351AE)',1,14,6,3,1),(4,'Мышь Logitech Wireless Mouse M185 (910-002238)',3,5,9,2,2),(5,'Мышь Logitech Wireless Mouse M185 (910-002238)',3,21,6,4,2),(7,'Клавиатура Speedlink Bedrock Keyboard USB (SL-6411-BK-RU)',2,7,7,3,2),(8,'wsedrtfgy',2,1,6,2,1),(9,'dgsdgsdg',2,8,6,2,1),(10,'23t4rtyulgfhdgdfs',2,3,9,4,3),(11,'uo\'i;uydf',2,5,6,4,2);

/*Table structure for table `mmove` */

DROP TABLE IF EXISTS `mmove`;

CREATE TABLE `mmove` (
  `mmove_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID перемещения расходного материала',
  `material_id` int(11) NOT NULL COMMENT 'ID материала',
  `mmove_count` int(11) NOT NULL COMMENT 'Количество материала',
  `mmove_officeold` int(11) NOT NULL COMMENT 'Откуда - Отделение',
  `mmove_departmentold` int(11) NOT NULL COMMENT 'Откуда - Департамент/отдел',
  `mmove_officenew` int(11) NOT NULL COMMENT 'Куда',
  `mmove_departmentnew` int(11) NOT NULL COMMENT 'Куда - Департамент/отдел',
  `mmove_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mmove_id`),
  KEY `material_id` (`material_id`),
  KEY `mmove_officeold` (`mmove_officeold`),
  KEY `mmove_officenew` (`mmove_officenew`),
  KEY `mmove_departmentold` (`mmove_departmentold`),
  KEY `mmove_departmentnew` (`mmove_departmentnew`),
  CONSTRAINT `mmove_ibfk_1` FOREIGN KEY (`material_id`) REFERENCES `material` (`material_id`),
  CONSTRAINT `mmove_ibfk_2` FOREIGN KEY (`mmove_officeold`) REFERENCES `office` (`office_id`),
  CONSTRAINT `mmove_ibfk_3` FOREIGN KEY (`mmove_officenew`) REFERENCES `office` (`office_id`),
  CONSTRAINT `mmove_ibfk_4` FOREIGN KEY (`mmove_departmentold`) REFERENCES `department` (`department_id`),
  CONSTRAINT `mmove_ibfk_5` FOREIGN KEY (`mmove_departmentnew`) REFERENCES `department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `mmove` */

insert  into `mmove`(`mmove_id`,`material_id`,`mmove_count`,`mmove_officeold`,`mmove_departmentold`,`mmove_officenew`,`mmove_departmentnew`,`mmove_date`) values (2,1,1,9,1,8,2,'0000-00-00 00:00:00'),(3,4,2,6,1,7,3,'0000-00-00 00:00:00'),(4,1,2,9,1,8,3,'0000-00-00 00:00:00'),(5,1,0,6,1,9,2,'0000-00-00 00:00:00'),(6,3,0,9,4,6,3,'0000-00-00 00:00:00'),(7,3,0,9,4,6,3,'0000-00-00 00:00:00'),(8,3,0,9,4,6,3,'0000-00-00 00:00:00'),(9,3,0,9,4,6,3,'0000-00-00 00:00:00'),(10,3,0,9,4,6,3,'0000-00-00 00:00:00'),(17,3,0,6,3,9,2,'2014-06-02 23:11:01'),(18,9,0,6,2,9,4,'2014-06-03 00:54:46'),(19,5,0,6,4,9,4,'2014-06-03 00:55:21'),(20,10,0,9,4,9,3,'2014-06-03 00:57:19'),(21,10,3,9,4,6,4,'2014-06-03 00:59:04'),(22,4,5,9,2,6,1,'2014-06-03 01:01:33');

/*Table structure for table `mtype` */

DROP TABLE IF EXISTS `mtype`;

CREATE TABLE `mtype` (
  `mtype_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID типа расходного материала',
  `mtype_name` varchar(50) NOT NULL COMMENT 'Наименование расходного материала',
  PRIMARY KEY (`mtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `mtype` */

insert  into `mtype`(`mtype_id`,`mtype_name`) values (1,'Картридж'),(2,'Клавиатура'),(3,'Мышь'),(4,'sdfg11');

/*Table structure for table `notification` */

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID уведомления',
  `notification_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата уведомления',
  `office_id` int(11) NOT NULL COMMENT 'ID отделения',
  `department_id` int(11) DEFAULT NULL COMMENT 'ID департамента/отдела',
  `notification_subject` varchar(255) NOT NULL COMMENT 'Заголовок письма',
  `notification_text` text NOT NULL COMMENT 'Текст уведомления',
  PRIMARY KEY (`notification_id`),
  KEY `office_id` (`office_id`),
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`office_id`) REFERENCES `office` (`office_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `notification` */

insert  into `notification`(`notification_id`,`notification_date`,`office_id`,`department_id`,`notification_subject`,`notification_text`) values (1,'2014-05-05 00:00:00',8,2,'','Уважаемые пользователи! ведомляем вас, что 20.05.2014 г. с 09:00 до 09:35 будет произведена замена оборудования. Все сетевые сервисы будут недоступны.'),(2,'2014-05-12 00:00:00',6,3,'','Уважаемые пользователи! Сбой в работе с сетевыми дисками устранен.'),(3,'2014-06-13 21:58:28',6,4,'1234567890-','1234567890-='),(4,'2014-06-13 21:59:58',6,2,'asdfghjkl;','asdfghjkl;\'\r\n'),(5,'2014-06-13 22:01:56',6,2,'sdfgjhkj;l\'','546789p]0\\-'),(6,'2014-06-13 22:02:35',6,2,'qqwertyuiop[','awertjykulg890i-\\='),(7,'2014-06-15 19:05:01',6,2,'12345678','qwertyuiop'),(8,'2014-06-15 20:18:49',6,2,'qwsdf','sfd'),(9,'2014-06-15 20:22:04',6,2,'qwsdf','sfd'),(10,'2014-06-15 20:22:53',6,2,'qwsdf','sfd'),(11,'2014-06-15 20:23:23',6,2,'qwsdf','sfd'),(12,'2014-06-15 20:23:55',6,2,'qwsdf','sfd'),(13,'2014-06-15 20:24:30',6,2,'qwsdf','sfd');

/*Table structure for table `office` */

DROP TABLE IF EXISTS `office`;

CREATE TABLE `office` (
  `office_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID отделения',
  `office_name` varchar(255) NOT NULL COMMENT 'Наименование отделения',
  `office_town` varchar(50) NOT NULL COMMENT 'Город',
  `office_adr` varchar(255) NOT NULL COMMENT 'Адрес',
  PRIMARY KEY (`office_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `office` */

insert  into `office`(`office_id`,`office_name`,`office_town`,`office_adr`) values (6,'Отделение №1','Запорожье','ул. Комарова,9'),(7,'Отделение №2','Запорожье','пр. Ленина, 126'),(8,'Отделение №10','Днепропетровск','ул. Ленина, 342'),(9,'Отделение №1','Киев','ул. Т.Шевченко,32'),(10,'qwerfgh1','sdfgh1','sdfghjk1'),(11,'3erghj','wegrt','dgnfmh');

/*Table structure for table `provider` */

DROP TABLE IF EXISTS `provider`;

CREATE TABLE `provider` (
  `provider_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID поставщика',
  `provider_name` varchar(255) NOT NULL COMMENT 'Наименование поставщика',
  `provider_town` varchar(50) NOT NULL COMMENT 'Город',
  `provider_adr` varchar(255) NOT NULL COMMENT 'Адрес',
  PRIMARY KEY (`provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `provider` */

insert  into `provider`(`provider_id`,`provider_name`,`provider_town`,`provider_adr`) values (1,'ООО \"ITsoft\"','г.Запорожье','пр. Ленина,218'),(2,'ЧП \"Кимов И.Л.\"','г. Киев','пр. К.Маркса,43'),(3,'ООО \"Юникс\"','г. Днепропетровск','ул. Правды,3'),(4,'1234567','1234567','1234567');

/*Table structure for table `repair` */

DROP TABLE IF EXISTS `repair`;

CREATE TABLE `repair` (
  `repair_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID ремонта/профилактики',
  `equipment_id` int(11) NOT NULL COMMENT 'ID оборудования',
  `repair_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '2-профилактика; 1-ремонт',
  `repair_startdate` date NOT NULL COMMENT 'Дата начала ремонта/профилактики',
  `repair_enddate` date NOT NULL COMMENT 'Дата окончания ремонта/профилактики',
  `provider_id` int(11) DEFAULT NULL COMMENT 'ID поставщика',
  `repair_reason` text COMMENT 'Причина ремонта',
  PRIMARY KEY (`repair_id`),
  KEY `equipment_id` (`equipment_id`),
  KEY `provider_id` (`provider_id`),
  CONSTRAINT `repair_ibfk_1` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`equipment_id`),
  CONSTRAINT `repair_ibfk_2` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `repair` */

insert  into `repair`(`repair_id`,`equipment_id`,`repair_type`,`repair_startdate`,`repair_enddate`,`provider_id`,`repair_reason`) values (1,16,1,'2014-01-02','2014-01-31',1,'22211');

/*Table structure for table `reqstatus` */

DROP TABLE IF EXISTS `reqstatus`;

CREATE TABLE `reqstatus` (
  `reqstatus_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID статуса заявки',
  `reqstatus_name` varchar(20) NOT NULL COMMENT 'Наименование статуса заявки',
  PRIMARY KEY (`reqstatus_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `reqstatus` */

insert  into `reqstatus`(`reqstatus_id`,`reqstatus_name`) values (1,'Принята'),(2,'Открыта'),(3,'На уточнении'),(4,'На удержании'),(5,'Закрыта');

/*Table structure for table `reqtype` */

DROP TABLE IF EXISTS `reqtype`;

CREATE TABLE `reqtype` (
  `reqtype_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID типа заявки',
  `reqtype_name` varchar(50) NOT NULL COMMENT 'Наименование типа заявки',
  PRIMARY KEY (`reqtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `reqtype` */

insert  into `reqtype`(`reqtype_id`,`reqtype_name`) values (1,'Консультация'),(2,'Нарушение в работе оборудования'),(3,'Замена расходного материала'),(4,'Нарушение в работе ПО');

/*Table structure for table `request` */

DROP TABLE IF EXISTS `request`;

CREATE TABLE `request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID заявки',
  `user_id` int(11) NOT NULL COMMENT 'ID пользователя',
  `request_crtdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата создания заявки',
  `reqtype_id` int(11) NOT NULL COMMENT 'ID  типа заявки',
  `mtype_id` int(11) DEFAULT NULL COMMENT 'ID расходного материала',
  `request_usercom` varchar(255) NOT NULL COMMENT 'Комментарий пользователя',
  `reqstatus_id` int(11) NOT NULL DEFAULT '2' COMMENT 'ID статуса заявки',
  `user_idadmin` int(11) DEFAULT NULL COMMENT 'ID админа',
  `request_clsdate` timestamp NULL DEFAULT NULL COMMENT 'Дата закрытия заявки',
  `request_admincom` varchar(255) DEFAULT NULL COMMENT 'Комментарий админа',
  PRIMARY KEY (`request_id`),
  KEY `user_id` (`user_id`),
  KEY `reqtype_id` (`reqtype_id`),
  KEY `mtype_id` (`mtype_id`),
  KEY `reqstatus_id` (`reqstatus_id`),
  KEY `user_idadmin` (`user_idadmin`),
  CONSTRAINT `request_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  CONSTRAINT `request_ibfk_2` FOREIGN KEY (`reqtype_id`) REFERENCES `reqtype` (`reqtype_id`),
  CONSTRAINT `request_ibfk_3` FOREIGN KEY (`mtype_id`) REFERENCES `mtype` (`mtype_id`),
  CONSTRAINT `request_ibfk_4` FOREIGN KEY (`reqstatus_id`) REFERENCES `reqstatus` (`reqstatus_id`),
  CONSTRAINT `request_ibfk_5` FOREIGN KEY (`user_idadmin`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `request` */

insert  into `request`(`request_id`,`user_id`,`request_crtdate`,`reqtype_id`,`mtype_id`,`request_usercom`,`reqstatus_id`,`user_idadmin`,`request_clsdate`,`request_admincom`) values (1,5,'2014-05-05 00:00:00',2,NULL,'Не знаю, как работать с Вордом! Тревога!!!',2,4,NULL,'asdfh'),(2,5,'2014-05-16 00:00:00',3,1,'Закончилась краска в принтере.',1,4,NULL,''),(8,5,'0000-00-00 00:00:00',3,2,'~!@#$%^&*()_+P{}L:\"M<>?1234567890-',2,NULL,NULL,NULL),(9,5,'0000-00-00 00:00:00',3,2,'~!@#$%^&*()_+P{}L:\"M<>?1234567890-',5,4,'2014-06-06 02:01:02','ertyuio'),(10,5,'0000-00-00 00:00:00',1,1,'xdfg',1,4,NULL,''),(11,5,'2014-06-03 19:29:24',3,1,'123456789',2,NULL,NULL,NULL),(12,5,'2014-06-05 23:05:49',3,1,'234567890',2,NULL,NULL,NULL),(15,5,'2014-06-06 02:13:53',1,NULL,'',5,4,'2014-06-06 02:16:00',''),(16,5,'2014-06-06 02:14:45',3,2,'',2,NULL,NULL,NULL),(17,5,'2014-06-13 22:11:33',3,1,'1qwerftgyhujikolp;\'\r\n',2,NULL,NULL,NULL),(18,5,'2014-06-13 22:12:25',3,1,'1qwerftgyhujikolp;\'\r\n',2,NULL,NULL,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID пользователя',
  `user_name` varchar(50) NOT NULL COMMENT 'Имя пользователя',
  `user_dol` varchar(255) NOT NULL COMMENT 'Должность',
  `office_id` int(11) NOT NULL COMMENT 'ID отделения',
  `department_id` int(11) NOT NULL COMMENT 'ID департамента/отдела',
  `user_mail` varchar(50) NOT NULL COMMENT 'Почтовый адрес пользователя',
  `user_pas` varchar(50) NOT NULL COMMENT 'Пароль',
  `user_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-пользователь; 1-админ',
  `user_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1-неактивен',
  PRIMARY KEY (`user_id`),
  KEY `office_id` (`office_id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`office_id`) REFERENCES `office` (`office_id`),
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`user_id`,`user_name`,`user_dol`,`office_id`,`department_id`,`user_mail`,`user_pas`,`user_flag`,`user_deleted`) values (4,'Комендяк Максим Владимирович','Инженер',9,1,'komendyakmv@bank.ua','f71156102d5ded50cd49c4690ba3add6',1,0),(5,'Иванова Мария Алексеевна','Бухгалтер',9,2,'ivanovama@bank.ua','f5268cec5bb9d03d2c19ac743b8f3f4b',0,0),(6,'Крот Игорь Николаевич','Экономист',7,3,'krotin@bank.ua','8a7e0f8f806930c5f2c576804b2ebdfe',0,0),(7,'Маслова Инна Ивановна','Кассир',6,2,'maslovaii@bank.ua','3a1a9f2a797d0ab2407f36c0c35a9886',0,0),(9,'Зайцев Роман Борисович','Экономист',9,3,'zaytsevrb@bank.ua','61e32fd391d6ea3638d35a50369d03d1',0,0),(11,'xxx','xxx',6,1,'xxx@xxx.com','02a243c4202b23e8ec78620f1ff48aa6',0,0),(12,'aaa1','aaa1',7,4,'aaa1@aaa1.com','f158f877b5c9c753da72152d26815452',0,0),(13,'sss','sss',6,2,'sss@ss.com','db6068696ae1d17f9e031e920f73e206',0,0),(14,'zz','zz',6,1,'zz@z.com','b379e30644c581d1dd41bd6c5f349c99',0,0),(15,'1234567890','1234567890',6,1,'1234567890@1234567890.com','e807f1fcf82d132f9bb018ca6738a19f',0,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
