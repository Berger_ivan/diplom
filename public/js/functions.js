function exportToExcel(event, sourceTable) {
    var id = 'temp_table_'+event.timeStamp;
    var tempTable  = $("<table/>");
    tempTable.attr('id',id);

    var tr = $("<tr/>");
    sourceTable.find("thead > tr:first > th").each(function(index, th){
        var newTh = $("<th/>");
        newTh.html($(th).html());
        tr.append(newTh);
    });
    tempTable.append(tr);
    sourceTable.find("tbody > tr").each(function(index, tr){
        var newTr = $("<tr/>");
        $(tr).find("td").each(function(index, td){
            newTr.append($("<td/>").text($(td).text()));
        });
        tempTable.append(newTr);
    });
    tempTable.hide().appendTo($('body'));
    tempTable.btechco_excelexport({
        containerid: id,
        datatype: $datatype.Table,
        ReturnUri: true
    });
    tempTable.remove();
}