<?php

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
//        $translator = $e->getApplication()->getServiceManager()->get('translator');
//        $translator
//            ->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']))
//            ->setFallbackLocale('ru_RU');

        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $e->getApplication()->getServiceManager()->get('Zend\Db\Adapter\Adapter');

        $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_DISPATCH, function ($e) {
            if ($e->getRouteMatch()->getMatchedRouteName() !== 'login') {
                if (!$e->getApplication()->getServiceManager()->get('Application\Model\AuthService')->hasIdentity()) {
                    return $e->getTarget()->plugin('redirect')->toRoute('login');
                }
            }
            $controller = $e->getTarget();

            $user = $e->getApplication()->getServiceManager()->get('Application\Model\AuthService')->getIdentity();
            if ($user && $e->getRouteMatch()->getMatchedRouteName() === 'login') {
                if ($user->isAdmin()) {
                    return $e->getTarget()->plugin('redirect')->toRoute('admin');
                } else {
                    return $e->getTarget()->plugin('redirect')->toRoute('home');
                }
            }
            if ($user && $user->isAdmin() && $this->getModuleName($e) !== 'Admin') {
                // Admin tried to open User's module
                $e->getTarget()->layout("admin404Layout");
            } else if ($user && !$user->isAdmin() && $this->getModuleName($e) === 'Admin') {
                // User tried to open Admin's module
                $e->getTarget()->layout("user404Layout");
            } else {
                if (__NAMESPACE__ === $this->getModuleName($e)) {
                    if (FALSE === $e->getViewModel()->terminate()) {
                        if ($e->getRouteMatch()->getMatchedRouteName() === 'login') {
                            $controller->layout("loginLayout");
                        } else {
                            $controller->layout(strtolower(__NAMESPACE__)."/layout");
                        }

                    }
                }
            }

            $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
            $viewModel->user = $user;

        });

//        \Locale::setDefault('ru_RU');
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories'=>array(
                'Application\Model\AuthStorage'=>function(\Zend\ServiceManager\ServiceManager $serviceManager){
                    return new \Application\Model\AuthStorage('diplom');
                },
                'Application\Model\AuthService'=>function(\Zend\ServiceManager\ServiceManager $serviceManager){
                    $dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter(
                        $dbAdapter, 'user', 'user_mail', 'user_pas', 'MD5(?)'
                    );
                    $authService = new \Zend\Authentication\AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($serviceManager->get('Application\Model\AuthStorage'));
                    return $authService;
                },
            ),
        );
    }

    protected function getModuleName(MvcEvent $e)
    {
        return substr(get_class($e->getTarget()), 0, strpos(get_class($e->getTarget()), "\\"));
    }

}