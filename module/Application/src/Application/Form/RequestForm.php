<?php

namespace Application\Form;

use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class RequestForm extends Form
{

    public function __construct()
    {
        parent::__construct('request_form');

        $this->setAttribute('method','post');

        $element = new Select();
        $element->setLabel('Тип заявки');
        $element->setName('reqtype_id');
        $element->setAttribute('id', $element->getName());
        $element->setValueOptions($this->getRequestTypes());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Тип расходного материала');
        $element->setName('mtype_id');
        $element->setAttribute('id', $element->getName());
        $element->setValueOptions($this->getMTypes());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Textarea();
        $element->setLabel('Комментарий инициатора');
        $element->setName('request_usercom');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new Submit();
        $element->setName('submit');
        $element->setValue('Создать');
        $this->add($element);

        $this->setInputFilter(new \Application\Form\Filter\RequestFormFilter());
    }

    protected function getRequestTypes()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT reqtype_id,reqtype_name FROM reqtype ORDER BY reqtype_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['reqtype_id']] = $res['reqtype_name'];
        }
        return $selectData;
    }

    protected function getMTypes()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT mtype_id,mtype_name FROM mtype ORDER BY mtype_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['mtype_id']] = $res['mtype_name'];
        }
        return $selectData;
    }

}