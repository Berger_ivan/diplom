<?php

namespace Application\Form\Filter;

class LoginFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_mail',
            'required' => true,
            'filters' => array(),
            'validators' => array(
                new \Zend\Validator\NotEmpty(),
                new \Zend\Validator\EmailAddress(),
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_pas',
            'required' => true,
            'filters' => array(),
            'validators' => array(
                new \Zend\Validator\NotEmpty(),
            ),
        ));

    }
}