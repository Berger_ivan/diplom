<?php

namespace Application\Form\Filter;

class RequestFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'reqtype_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
                new \Zend\Validator\NotEmpty(),
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'mtype_id',
            'required' => false,
            'filters' => array(),
            'validators' => array(
                new \Zend\Validator\NotEmpty(),
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'request_usercom',
            'required' => false,
            'filters' => array(),
            'validators' => array(
                new \Zend\Validator\NotEmpty(),
            ),
        ));

    }
}