<?php

namespace Application\Form\Filter;

use Zend\Validator\Identical;

class UserProfileFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_pas',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_pas_2',
            'required' => true,
            'filters' => array(),
            'validators' => array(
                new Identical('user_pas'),
            ),
        ));

    }
}