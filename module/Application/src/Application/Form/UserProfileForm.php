<?php

namespace Application\Form;

use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class UserProfileForm extends Form
{

    public function __construct()
    {
        parent::__construct('profile_form');

        $this->setAttribute('method','post');

//        $user_name = new Text();
//        $user_name->setLabel('ФИО');
//        $user_name->setName('user_name');
//        $this->add($user_name);
//
//        $user_dol = new Text();
//        $user_dol->setLabel('Должность');
//        $user_dol->setName('user_dol');
//        $this->add($user_dol);
//
//        $office_id = new Select();
//        $office_id->setLabel('Отделение');
//        $office_id->setName('office_id');
//        $office_id->setOptions($this->getOptionsForSelect());
//        $this->add($office_id);
//
//        $user_mail = new Email();
//        $user_mail->setLabel('E-mail');
//        $user_mail->setName('user_mail');
//        $this->add($user_mail);

        $user_pass = new Password();
        $user_pass->setLabel('Новый пароль');
        $user_pass->setName('user_pas');
        $this->add($user_pass);

        $user_pass_2 = new Password();
        $user_pass_2->setLabel('Повторение нового пароля');
        $user_pass_2->setName('user_pas_2');
        $this->add($user_pass_2);

        $submit = new Submit();
        $submit->setName('submit');
        $submit->setValue('Сохранить');
        $this->add($submit);

        $this->setInputFilter(new \Application\Form\Filter\UserProfileFormFilter());
    }

//    protected function getOptionsForSelect()
//    {
//        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
//        $sql = 'SELECT office_id,office_name FROM office ORDER BY office_name';
//        $statement = $dbAdapter->query($sql);
//        $result = $statement->execute();
//
//        $selectData = array();
//
//        foreach ($result as $res) {
//            $selectData[$res['office_id']] = $res['office_name'];
//        }
//        return $selectData;
//    }

}