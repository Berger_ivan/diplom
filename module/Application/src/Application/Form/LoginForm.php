<?php

namespace Application\Form;

use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class LoginForm extends Form
{

    public function __construct()
    {
        parent::__construct('login_form');

        $this->setAttribute('method','post');

        $email = new Email();
        $email->setLabel('Email');
        $email->setName('user_mail');
        $this->add($email);

        $password = new Password();
        $password->setLabel('Password');
        $password->setName('user_pas');
        $this->add($password);

        $remember = new Checkbox();
        $remember->setLabel('Remember me');
        $remember->setName('remember');
        $this->add($remember);

        $submit = new Submit();
        $submit->setName('submit');
        $submit->setValue('Submit');
        $this->add($submit);

        $this->setInputFilter(new \Application\Form\Filter\LoginFormFilter());
    }
}