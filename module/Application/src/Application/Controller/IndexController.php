<?php

namespace Application\Controller;

use Admin\Model\MatsModel;
use Application\Form\RequestForm;
use Application\Form\UserProfileForm;
use Application\Model\EquipmentModel;
use Zend\View\Model\ViewModel;
use Application\Form\LoginForm;

class IndexController extends \Application\Controller\AbstractController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();

        if ($this->getAuthService()->hasIdentity()) {
            $userId = $this->getAuthService()->getIdentity()->getUserId();
            $equipmentModel = new EquipmentModel();
            $viewModel->setVariable('equipments', $equipmentModel->getEquipmentByUser($userId));
        }

        return $viewModel;
    }

    public function requestsAction()
    {
        $viewModel = new ViewModel();

        if ($this->getAuthService()->hasIdentity()) {
            $userId = $this->getAuthService()->getIdentity()->getUserId();
            $requestModel = new \Application\Model\RequestModel();
            $requests = $requestModel->getByUserId($userId);
            $viewModel->setVariable('requests', $requests);
        }

        return $viewModel;
    }

    public function newRequestAction()
    {
        $viewModel = new ViewModel();

        $form = new RequestForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($this->getRequest()->getPost('reqtype_id', null) == 3) {
                $form->getInputFilter()->get('mtype_id')->setRequired(true);
            }

            if ($form->isValid()) {
                if ($this->getAuthService()->hasIdentity()) {
                    $requestModel = new \Application\Model\RequestModel();
                    $requestModel->createRequest($form->getData(), $this->getAuthService()->getStorage()->read()->getUserId());
                    return $this->redirect()->toRoute('home/default', array('action'=>'requests'));
                }
            }
        }

        $viewModel->setVariable('form', $form);
        return $viewModel;
    }

    public function profileAction()
    {
        $viewModel = new ViewModel();

        $form = new UserProfileForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                if ($this->getAuthService()->hasIdentity()) {
                    $profileModel = new \Application\Model\ProfileModel();
                    $result = $profileModel->updatePassword($this->getAuthService()->getStorage()->read()->getUserId(),$form->get('user_pas')->getValue());
                    if ($result) {
                        $message = 'Пароль успешно изменён';
                    } else {
                        $message = 'Новый пароль совпадает с текущим';
                    }
                    $viewModel->setVariable('message', $message);
                }
            }
        }


        $viewModel->setVariable('form', $form);

        $usersModel = new \Application\Model\UsersModel();
        if ($this->getAuthService()->hasIdentity()) {
            $userObj = $this->getAuthService()->getStorage()->read();
            $viewModel->setVariable('user', $usersModel->getUser($userObj->getUserId()));
        }
        return $viewModel;
    }

    public function loginAction()
    {
        $viewModel = new ViewModel();
        $form = new LoginForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getAuthService()->getAdapter()
                    ->setIdentityColumn('user_mail')
                    ->setCredentialColumn('user_pas')
                    ->setIdentity($form->get('user_mail')->getValue())
                    ->setCredential($form->get('user_pas')->getValue());

                $result = $this->getAuthService()->authenticate();
                if ($result->isValid()) {
                    if ($request->getPost('rememberme') == 1 ) {
                        $this->getAuthStorage()->setRememberMe(1);
                        $this->getAuthService()->setStorage($this->getAuthStorage());
                    }
                    $userRowData = (array)$this->getAuthService()->getAdapter()->getResultRowObject();
                    $user = new \Application\Model\Entity\User();
                    $userData = $user->exchangeArray($userRowData);
                    $this->getAuthService()->getStorage()->write($userData);
                    if ($userData->isAdmin()) {
                        return $this->redirect()->toRoute('admin');
                    }
                    return $this->redirect()->toRoute('home');
                } else {
                    foreach ($result->getMessages() as $message) {
                        $this->flashmessenger()->addMessage($message);
                    }
                }
            }
        }
        $viewModel->setVariable('form', $form);
        $viewModel->setVariable('messages', $this->flashmessenger()->getMessages());
        return $viewModel;
    }

    public function logoutAction()
    {
        $this->getAuthStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        $this->flashmessenger()->addMessage("You've been logged out");
        return $this->redirect()->toRoute('login');
    }

}
