<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class AbstractController extends AbstractActionController
{

    protected $_authStorage;
    protected $_authService;



    /**
     * @return \Zend\Authentication\AuthenticationService
     */
    public function getAuthService()
    {
        if (!$this->_authService) {
            $this->_authService = $this->getServiceLocator()->get('Application\Model\AuthService');
        }
        return $this->_authService;
    }

    /**
     * @return \Application\Model\AuthStorage
     */
    public function getAuthStorage()
    {
        if (!$this->_authStorage) {
            $this->_authStorage = $this->getServiceLocator()->get('Application\Model\AuthStorage');
        }
        return $this->_authStorage;
    }

    public function getModel($modelName)
    {
        $model = "Application\\Model\\".ucfirst($modelName)."Model";
        return new $model();
    }

}