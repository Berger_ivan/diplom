<?php

namespace Application\Model;

use Application\Model\Table;

class EqtypeModel
{
    public function getAllEqtypes()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT * FROM eqtype ORDER BY eqtype.eqtype_id';
        return $dbAdapter->query($sql)->execute();
    }

    /**
     * @return array
     */
    public function getEqtypesForSelect()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT eqtype.eqtype_id, eqtype.eqtype_name'
            .' FROM eqtype ORDER BY eqtype.eqtype_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['eqtype_id']] = $res['eqtype_name'];
        }
        return $selectData;
    }

    public function getEqtypeById($id)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = " SELECT * FROM `eqtype` WHERE eqtype_id = " . (int)$id;
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();
    }

    public function updateEqtype($eqtypeId, $eqtypeName)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            UPDATE `eqtype`
            SET `eqtype`.`eqtype_name` = ".$dbAdapter->getPlatform()->quoteValue($eqtypeName)."
            WHERE `eqtype_id` = ".(int)$eqtypeId;

        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

    public function createEqtype($eqtypeName)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "INSERT INTO `eqtype` (`eqtype_name`) VALUES (".$p->quoteValue($eqtypeName).")";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

}