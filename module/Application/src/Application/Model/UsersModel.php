<?php

namespace Application\Model;

use Application\Model\Table\Tables;
use Application\Model\Table;

class UsersModel
{

    /**
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function getAllUsers()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "SELECT
            `user`.user_id as user_id,
            `user`.user_name as user_name,
            `user`.user_dol as user_dol,
            `user`.user_mail as user_mail,
            CONCAT(office.office_name,' ',office.office_town,' ',office.office_adr) as office,
            department.department_name as department_name
            FROM `user`
            LEFT JOIN office ON `user`.office_id=office.office_id
            LEFT JOIN department ON `user`.department_id=department.department_id
            ";
        $result = $dbAdapter->query($sql)->execute();
        return $result;
    }

    public function getUsersForSelect()
    {
        $result = array();
        foreach ($this->getAllUsers() as $user) {
            $result[$user['user_id']] = $user['user_name'];
        }
        return $result;
    }

    public function getUsersByOfficeIdAndDepartmentId($officeId, $departmentId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "SELECT * FROM `user`
            WHERE office_id=".(int)$officeId."
            AND department_id=".(int)$departmentId."
            ";
        $select = array();
        foreach ($dbAdapter->query($sql)->execute() as $user) {
            $select[$user['user_id']] = $user['user_name'];
        }
        return $select;
    }

    /**
     * @param $userId
     * @return null|array
     */
    public function getUser($userId)
    {
        $sql = '
            SELECT
            `user`.user_name as user_name,
            `user`.user_dol as user_dol,
            `user`.user_mail as user_mail,
            `user`.office_id as office_id,
            `user`.department_id as department_id,
            `user`.user_pas as user_pas,
            CONCAT(office.office_name," ",office.office_town," ",office.office_adr) as office,
            department.department_name as department_name
            FROM `user`
            LEFT JOIN office ON `user`.office_id=office.office_id
            LEFT JOIN department ON `user`.department_id=department.department_id
            WHERE user.user_id='.$userId;
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();

//        $rows = Tables::getUsersTable()->select(array('id'=>$userId));
//        return $rows->count() == 0 ? null : $rows->current();
    }


    /**
     * @param $userId
     * @return bool
     */
    public function isAdmin($userId)
    {
        return !Tables::getUsersTable()->select(array('user_id'=>$userId, 'is_admin'=>'1'))->current() ? false : true;
    }

    public function createUser($userData)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "INSERT INTO `user` (`user_name`,`user_dol`,`office_id`, `department_id`, `user_mail`,`user_pas`) VALUES
            (".$p->quoteValue($userData['user_name']).", ".$p->quoteValue($userData['user_dol']).", ".(int)$userData['office_id'].", ".(int)$userData['department_id'].", ".$p->quoteValue($userData['user_mail']).", '".MD5($userData['user_pas'])."')";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

    public function updateUser($userId, $userData)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "UPDATE `user` SET ";
        $sqlParams = array();
        if (!empty($userData['user_name'])) $sqlParams[] = " `user_name` = ".$p->quoteValue($userData['user_name']);
        if (!empty($userData['user_dol'])) $sqlParams[] = " `user_dol` = ".$p->quoteValue($userData['user_dol']);
        if (!empty($userData['user_mail'])) $sqlParams[] = " `user_mail` = ".$p->quoteValue($userData['user_mail']);
        if (!empty($userData['office_id'])) $sqlParams[] = " `office_id` = ".(int)$userData['office_id'];
        if (!empty($userData['department_id'])) $sqlParams[] = " `department_id` = ".(int)$userData['department_id'];
        if (!empty($userData['user_pas'])) $sqlParams[] = " `user_pas` = '".MD5($userData['user_pas'])."'";
        $sql .= implode(', ', $sqlParams);
        $sql .= " WHERE `user_id` = ".(int)$userId;

        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

    public function getUsersForNotification($officeId, $departamentId = null)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "SELECT * FROM `user` WHERE office_id = " . (int)$officeId;
        if ($departamentId) {
            $sql .= " AND department_id = " . (int)$departamentId;
        }
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result;
    }

}