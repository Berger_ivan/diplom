<?php

namespace Application\Model;

use Application\Model\Table;

class EquipmentModel
{
    /**
     * @return array
     */
    public function getEquipmentTypesForSelect()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT
            `eqtype`.`eqtype_id` as `id`,
            `eqtype`.`eqtype_name` as `name`
            FROM `eqtype`
            ORDER BY `name`
            ';
        $result = array();
        foreach ($dbAdapter->query($sql)->execute() as $item) {
            $result[$item['id']] = $item['name'];
        }
        return $result;
    }



    public function getEquipmentsByOfficeIdAndDepartmentId($oldOfficeId, $oldDepartmentId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT
            equipment.equipment_id,
            equipment.equipment_name
            FROM equipment
            LEFT JOIN office ON office.office_id = equipment.office_id
            LEFT JOIN department ON department.department_id = equipment.department_id
            WHERE equipment.office_id = '.(int)$oldOfficeId.' AND equipment.department_id = '.(int)$oldDepartmentId.'
            ORDER BY equipment.equipment_name';
        $result = $dbAdapter->query($sql)->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['equipment_id']] = $res['equipment_name'];
        }
        return $selectData;
    }



    /**
     * @return array
     */
    public function getEquipmentsForSelect()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT
            equipment.equipment_id,
            equipment.equipment_name,
            office.office_id,
            CONCAT(office.office_name,", ",office.office_town,", ",office.office_adr) as office_name,
            department.department_id,
            department.department_name
            FROM equipment
            LEFT JOIN office ON office.office_id = equipment.office_id
            LEFT JOIN department ON department.department_id = equipment.department_id
            ORDER BY equipment.equipment_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function getAllEquipments()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            SELECT
            `equipment`.`equipment_invnom`
            ,`equipment`.equipment_id
            ,`equipment`.equipment_name
            ,`eqtype`.`eqtype_name`
            ,`equipment`.equipment_characteristics
            ,`equipment`.equipment_crtdate
            ,`equipment`.equipment_warrdate
            ,`equipment`.equipment_life
            ,`equipment`.equipment_cost
            ,CONCAT(`office`.`office_name`,' ',`department`.`department_name`) AS office
            ,`user`.`user_name`
            ,`provider`.`provider_name`
            ,CASE WHEN `equipment`.`equipment_flagrepair`=1 THEN 'Ремонт'
                  WHEN `equipment`.`equipment_flagrepair`=1 THEN 'Профилактика'
            END AS equipment_repair
            FROM `equipment`
            INNER JOIN `eqtype` ON `equipment`.`eqtype_id`=`eqtype`.`eqtype_id`
            INNER JOIN `office` ON `equipment`.`office_id`=`office`.`office_id`
            INNER JOIN `department` ON `equipment`.`department_id`=`department`.`department_id`
            LEFT JOIN `user` ON `equipment`.`user_id`=`user`.`user_id`
            LEFT JOIN `provider` ON `equipment`.`provider_id`=`provider`.`provider_id`
            WHERE equipment_flagremote=0
            ORDER BY equipment_id DESC, equipment_name ASC
        ";
        $result = $dbAdapter->query($sql)->execute();
        return $result;
    }

    public function getById($id)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = " SELECT * FROM `equipment` WHERE equipment_id = " . (int)$id;
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();
    }
    /**
     * @param $userId
     * @return null|array
     */
//    public function getUser($userId)
//    {
//        $sql = '
//            SELECT
//            `user`.user_name as user_name,
//            `user`.user_dol as user_dol,
//            `user`.user_mail as user_mail,
//            `user`.office_id as office_id,
//            `user`.department_id as department_id,
//            `user`.user_pas as user_pas,
//            CONCAT(office.office_name," ",office.office_town," ",office.office_adr) as office,
//            department.department_name as department_name
//            FROM `user`
//            LEFT JOIN office ON `user`.office_id=office.office_id
//            LEFT JOIN department ON `user`.department_id=department.department_id
//            WHERE user.user_id='.$userId;
//        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
//        $result = $dbAdapter->query($sql)->execute();
//        return $result->count() == 0 ? null : $result->current();
//    }


    public function createEquipment($userData)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            INSERT INTO `equipment` (
            `equipment_name`,
            `equipment_invnom`,
            `eqtype_id`,
            `equipment_characteristics`,
            `equipment_crtdate`,
            `equipment_warrdate`,
            `equipment_life`,
            `equipment_cost`,
            `office_id`,
            `department_id`,
            `user_id`,
            `provider_id`
            ) VALUES (
            ".$p->quoteValue($userData['equipment_name']).",
            ".$p->quoteValue($userData['equipment_invnom']).",
            ".(int)$userData['eqtype_id'].",
            ".$p->quoteValue($userData['equipment_characteristics']).",
            ".$p->quoteValue($userData['equipment_crtdate']).",
            ".$p->quoteValue($userData['equipment_warrdate']).",
            ".$p->quoteValue($userData['equipment_life']).",
            ".$userData['equipment_cost'].",
            ".(int)$userData['office_id'].",
            ".(int)$userData['department_id'].",
            ".(int)$userData['user_id'].",
            ".(int)$userData['provider_id']."
            );
            ";

        $result = $dbAdapter->query($sql)->execute();
        return $result->getGeneratedValue();
    }

    public function updateEquipment($id, $data)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "UPDATE `equipment` SET ";
        $sqlParams = array();
        if (!empty($data['equipment_name'])) $sqlParams[] = " `equipment_name` = ".$p->quoteValue($data['equipment_name']);
        if (!empty($data['equipment_invnom'])) $sqlParams[] = " `equipment_invnom` = ".$p->quoteValue($data['equipment_invnom']);
        if (!empty($data['eqtype_id'])) $sqlParams[] = " `eqtype_id` = ".(int)$data['eqtype_id'];
        if (!empty($data['equipment_characteristics'])) $sqlParams[] = " `equipment_characteristics` = ".$p->quoteValue($data['equipment_characteristics']);
        if (!empty($data['equipment_crtdate'])) $sqlParams[] = " `equipment_crtdate` = ".$p->quoteValue($data['equipment_crtdate']);
        if (!empty($data['equipment_warrdate'])) $sqlParams[] = " `equipment_warrdate` = ".$p->quoteValue($data['equipment_warrdate']);
        if (!empty($data['equipment_life'])) $sqlParams[] = " `equipment_life` = ".$p->quoteValue($data['equipment_life']);
        if (!empty($data['office_id'])) $sqlParams[] = " `office_id` = ".(int)$data['office_id'];
        if (!empty($data['department_id'])) $sqlParams[] = " `department_id` = ".(int)$data['department_id'];

        if (!empty($data['user_id'])) {
            $sqlParams[] = " `user_id` = " . (int)$data['user_id'];
        } else {
            $sqlParams[] = " `user_id` = NULL";
        }

        if (!empty($data['provider_id'])) $sqlParams[] = " `provider_id` = ".(int)$data['provider_id'];
        if (!empty($data['equipment_flagremote'])) $sqlParams[] = " `equipment_flagremote` = ".(int)$data['equipment_flagremote'];

        if (!empty($data['equipment_flagrepair'])) {
            $sqlParams[] = " `equipment_flagrepair` = ".(int)$data['equipment_flagrepair'];
        } else {
            $sqlParams[] = " `equipment_flagrepair` = '0'";
        }

        if (!empty($data['equipment_cost'])) $sqlParams[] = " `equipment_cost` = ".$data['equipment_cost'];
        $sql .= implode(', ', $sqlParams);
        $sql .= " WHERE `equipment_id` = ".(int)$id;
//        echo $sql;die;
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

    public function setRemote($equipmentId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            INSERT INTO `eqremote`
            (`equipment_id`)
            VALUES
            (".(int)$equipmentId.")
        ";
        $dbAdapter->query($sql)->execute();

        $sql = "
            UPDATE `equipment`
            SET `equipment_flagremote` = 1
            WHERE `equipment_id` = ".(int)$equipmentId."
        ";
        $resultSet = $dbAdapter->query($sql)->execute();

        return $resultSet->getAffectedRows();
    }

    public function getRepair()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            SELECT
            `repair_id`,
            `repair_startdate`,
            `repair_enddate`,
            `repair_reason`,
            `equipment`.`equipment_name`
            FROM `repair`
            LEFT JOIN `equipment` ON `equipment`.`equipment_id`=`repair`.`equipment_id`
        ";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

    public function getRemote()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            SELECT
            `eqremote`.`equipment_id`,
            `eqremote_date`,
            `equipment`.`equipment_name`
            FROM `eqremote`
            LEFT JOIN `equipment` ON `equipment`.`equipment_id`=`eqremote`.`equipment_id`
        ";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

    public function getEquipmentMovementHistory()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            SELECT
            `eqmove`.`eqmove_id`,
            `equipment`.`equipment_name`,
            `eqmove`.`eqmove_date`,
            `eqmove`.`equipment_invnom`,
            `old_user`.`user_name` as `old_user_name`,
            `new_user`.`user_name` as `new_user_name`,
            `office1`.`office_name` as `office_name_old`,
            `office2`.`office_name` as `office_name_new`,
            `department1`.`department_name` as `department_name_old`,
            `department2`.`department_name` as `department_name_new`
            FROM `eqmove`
            LEFT JOIN `equipment` ON `equipment`.`equipment_id`=`eqmove`.`equipment_id`
            LEFT JOIN `office` as `office1` ON `office1`.`office_id`=`eqmove`.`eqmove_officeold`
            LEFT JOIN `office` as `office2` ON `office2`.`office_id`=`eqmove`.`eqmove_officenew`
            LEFT JOIN `department` as `department1` ON `department1`.`department_id`=`eqmove`.`eqmove_departmentold`
            LEFT JOIN `department` as `department2` ON `department2`.`department_id`=`eqmove`.`eqmove_departmentnew`
            LEFT JOIN `user` as `old_user` ON `old_user`.`user_id`=`eqmove`.`old_user_id`
            LEFT JOIN `user` as `new_user` ON `new_user`.`user_id`=`eqmove`.`new_user_id`
        ";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

    public function moveEquipment($equipmentId, $officeId, $departmentId, $newUserId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            INSERT INTO `accequip`.`eqmove` (
            `equipment_id`,
            `equipment_invnom`,
            `eqmove_officeold`,
            `eqmove_departmentold`,
            `old_user_id`,
            `eqmove_officenew`,
            `eqmove_departmentnew`,
            `new_user_id`
            ) VALUES (
             ".(int)$equipmentId.",
             (SELECT equipment_invnom FROM equipment WHERE equipment.equipment_id = ".(int)$equipmentId."),
             (SELECT office_id FROM equipment WHERE equipment.equipment_id = ".(int)$equipmentId."),
             (SELECT department_id FROM equipment WHERE equipment.equipment_id = ".(int)$equipmentId."),
             (SELECT user_id FROM equipment WHERE equipment.equipment_id = ".(int)$equipmentId."),
             ".(int)$officeId.",
             ".(int)$departmentId.",
             ".(int)$newUserId."
            )";
        $resultSet = $dbAdapter->query($sql)->execute();


        $sql = "
            UPDATE equipment
            SET
            office_id = ".(int)$officeId.",
            department_id = ".(int)$departmentId.",
            user_id = ".(int)$newUserId."
            WHERE equipment_id = ".(int)$equipmentId."
        ";
        $resultSet = $dbAdapter->query($sql)->execute();

        return $resultSet->getAffectedRows();
    }

    public function getEquipmentByUser($userId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = '
            SELECT
            equipment.equipment_name
            ,equipment_characteristics
            ,equipment_warrdate
            ,equipment_life
            FROM equipment
            WHERE equipment.`equipment_id` NOT IN (SELECT eqremote.`equipment_id` FROM eqremote ) AND equipment.user_id='.(int)$userId;
        return $dbAdapter->query($sql)->execute();

    }

    public function createRepair($equipmentId, $repair_type, $repair_startdate, $repair_enddate, $provider_id, $repair_reason)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            INSERT INTO `accequip`.`repair`
            (`equipment_id`, `repair_type`, `repair_startdate`, `repair_enddate`, `provider_id`, `repair_reason`)
            VALUES
            (
                ".(int)$equipmentId."
                ,".(int)$repair_type."
                ,".$p->quoteValue($repair_startdate)."
                ,".$p->quoteValue($repair_enddate)."
                ,".(int)$provider_id."
                ,".$p->quoteValue($repair_reason)."
            )
        ";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->getAffectedRows();
    }

    public function updateRepair($repairID, $equipmentId, $repair_type, $repair_startdate, $repair_enddate, $provider_id, $repair_reason)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "UPDATE `repair` SET ";
        $sqlParams = array();
        if (!empty($equipmentId)) $sqlParams[] = " `equipment_id` = ".(int)$equipmentId;
        if (!empty($repair_type)) $sqlParams[] = " `repair_type` = ".(int)$repair_type;
        if (!empty($repair_startdate)) $sqlParams[] = " `repair_startdate` = ".$p->quoteValue($repair_startdate);
        if (!empty($repair_enddate)) $sqlParams[] = " `repair_enddate` = ".$p->quoteValue($repair_enddate);
        if (!empty($provider_id)) $sqlParams[] = " `provider_id` = ".(int)$provider_id;
        if (!empty($repair_reason)) $sqlParams[] = " `repair_reason` = ".$p->quoteValue($repair_reason);
        $sql .= implode(', ', $sqlParams);
        $sql .= " WHERE `repair_id` = ".(int)$repairID;
//        echo $sql;die;
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

    public function getRepairByEquipmentId($equipmentId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT * FROM repair WHERE repair.equipment_id = '.(int)$equipmentId;
        return $dbAdapter->query($sql)->execute()->current();
    }

}