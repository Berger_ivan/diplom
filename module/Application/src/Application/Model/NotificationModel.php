<?php

namespace Application\Model;

use Application\Model\Table;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;


class NotificationModel
{
    public function getAll()
    {
        $sql = '
            SELECT
            `notification_id`,
            `notification_date`,
            CONCAT(office.office_name,", ",office.office_town,", ",office.office_adr) as office_name,
            `department`.`department_name`,
            `notification_subject`,
            `notification_text`
            FROM `notification`
            LEFT JOIN `office` ON `notification`.`office_id`=`office`.`office_id`
            LEFT JOIN `department` ON `notification`.`department_id`=`department`.`department_id`
        ';
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        return $dbAdapter->query($sql)->execute();

    }

    public function send($to, $from, $subject, $bodyHtml)
    {
        $message = new Message();
        $message->addTo($to)
            ->addFrom($from)
            ->setSubject($subject);

        $transport = new SmtpTransport();
        $options   = new SmtpOptions(array(
            'host'=>'smtp.gmail.com',
            'connection_class'=>'login',
            'connection_config'=>array(
                'ssl'=>'tls',
                'username'=>'sergeikos88',
                'password'=>'Zaq12wsxc'
            ),
            'port'=>587,
        ));

        $html = new MimePart($bodyHtml);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $message->setBody($body);

        $transport->setOptions($options);
        $transport->send($message);
    }

    public function createNotification($adminMail, $data, $fakeEmail = null)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        if (!empty($data['department_id'])) {
            $data['department_id'] = (int)$data['department_id'];
        } else {
            $data['department_id'] = "'NULL'";
        }
        $sql = "
            INSERT INTO `notification` (
            office_id,
            department_id,
            notification_subject,
            notification_text
            ) VALUES (
            ".(int)$data['office_id'].",
            ".$data['department_id'].",
            ".$p->quoteValue($data['notification_subject']).",
            ".$p->quoteValue($data['notification_text'])."
            )
        ";
        $result = $dbAdapter->query($sql)->execute();

        $userModel = new UsersModel();

        $users = $userModel->getUsersForNotification((int)$data['office_id'], $data['department_id']);
        if ($users) {
            $from = $adminMail;
            $bodyHtml = $data['notification_text'];
            $subject = $data['notification_subject'];
            foreach ($users as $user) {
                $to = $user['user_mail'];
                if ($fakeEmail) {
                    $to = $fakeEmail;
                }
                $this->send($to, $from, $subject, $bodyHtml);
            }
        }

        return $result->getGeneratedValue();
    }

}