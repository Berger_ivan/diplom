<?php

namespace Application\Model;

use Application\Model\Table;

class MtypeModel
{
    public function getAllMtypes()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT * FROM mtype ORDER BY mtype.mtype_id';
        return $dbAdapter->query($sql)->execute();
    }

    /**
     * @return array
     */
    public function getMtypesForSelect()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT mtype.mtype_id, mtype.mtype_name'
            .' FROM mtype ORDER BY mtype.mtype_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['mtype_id']] = $res['mtype_name'];
        }
        return $selectData;
    }

    public function getMtypeById($id)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = " SELECT * FROM `mtype` WHERE mtype_id = " . (int)$id;
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();
    }

    public function updateMtype($mtypeId, $mtypeName)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            UPDATE `mtype`
            SET `mtype`.`mtype_name` = ".$dbAdapter->getPlatform()->quoteValue($mtypeName)."
            WHERE `mtype_id` = ".(int)$mtypeId;

        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

    public function createMtype($mtypeName)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "INSERT INTO `mtype` (`mtype_name`) VALUES (".$p->quoteValue($mtypeName).")";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

}