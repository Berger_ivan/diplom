<?php

namespace Application\Model\Table;

use Zend\Stdlib\Hydrator\HydratorInterface;

class AbstractTable extends \Zend\Db\TableGateway\TableGateway
{

    public function __construct($tableName, $objectPrototype, HydratorInterface $hydrator = null)
    {
        if (!$hydrator) {
            $hydrator = new \Zend\Stdlib\Hydrator\ClassMethods;
        }
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $resultSet = new \Zend\Db\ResultSet\HydratingResultSet($hydrator, $objectPrototype);
        parent::__construct($tableName, $dbAdapter, null, $resultSet);
    }

}