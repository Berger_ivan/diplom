<?php

namespace Application\Model\Table;

class Tables
{

    const TABLE_NAME_USER = 'user';
    const TABLE_NAME_REQUEST = 'request';
//    const TABLE_NAME_DEPARTMENT = 'department';

    /**
     * @var \Application\Model\Table\AbstractTable
     */
    protected static $usersTable;
    protected static $requestTable;

    /**
     * @return \Application\Model\Table\AbstractTable
     */
    static function getUsersTable()
    {
        if (!static::$usersTable) {
            static::$usersTable = new \Application\Model\Table\AbstractTable(
                self::TABLE_NAME_USER
                ,new \Application\Model\Entity\User()
                ,new \Zend\Stdlib\Hydrator\ClassMethods()
            );
        }
        return static::$usersTable;
    }

    /**
     * @return \Application\Model\Table\AbstractTable
     */
    static function getRequestTable()
    {
        if (!static::$departmentTable) {
            static::$departmentTable = new \Application\Model\Table\AbstractTable(
                self::TABLE_NAME_DEPARTMENT
                ,new \Application\Model\Entity\Request()
                ,new \Zend\Stdlib\Hydrator\ClassMethods()
            );
        }
        return static::$departmentTable;
    }

}