<?php

namespace Application\Model;

use Application\Model\Table\Tables;
use Application\Model\Table;
use Zend\Db\Adapter\Adapter;

class RequestModel
{

    /**
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function getAll()
    {
        return Tables::getRequestTable()->select();
    }

    public function getById($requestId)
    {
        $sql = 'SELECT * FROM `request` WHERE request.request_id = '.(int)$requestId;
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();
    }

    public function updateRequest($requestId, $data)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "UPDATE `request` SET ";
        $sqlParams = array();

        if (!empty($data['request_admincom'])) $sqlParams[] = " `request_admincom` = ".$p->quoteValue($data['request_admincom']);
        if (!empty($data['reqtype_id'])) $sqlParams[] = " `reqtype_id` = ".(int)$data['reqtype_id'];
        if (!empty($data['reqstatus_id'])) {
            $sqlParams[] = " `reqstatus_id` = ".(int)$data['reqstatus_id'];
            if ((int)$data['reqstatus_id'] == 5) {
                $sqlParams[] = " `request_clsdate` = CURRENT_TIMESTAMP";
            } else {
                $sqlParams[] = " `request_clsdate` = NULL";
            }
        }
        if (!empty($data['user_idadmin'])) $sqlParams[] = " `user_idadmin` = ".(int)$data['user_idadmin'];
        $sqlParams[] = " `request_admincom` = '". (!empty($data['request_admincom']) ?  (string)$data['request_admincom'] : '') . "'";

        $sql .= implode(', ', $sqlParams);
        $sql .= " WHERE `request_id` = ".(int)$requestId;
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

    public function getForAdmin($userId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = '
            SELECT
            request.request_id
            ,user.user_name
            ,office.`office_id`
            ,request.request_crtdate
            ,reqtype.reqtype_name
            ,mtype.mtype_name
            ,request.request_usercom
            ,reqstatus.reqstatus_name
            ,admin.user_name as admin_name
            ,request.request_clsdate
            ,request.request_admincom
            FROM request
            LEFT JOIN `user` ON request.user_id=user.user_id
            LEFT JOIN `office` ON `user`.`office_id`=office.`office_id`
            LEFT JOIN reqtype ON request.reqtype_id=reqtype.reqtype_id
            LEFT JOIN mtype ON request.mtype_id=mtype.mtype_id
            LEFT JOIN reqstatus ON request.reqstatus_id=reqstatus.reqstatus_id
            LEFT JOIN `user`  admin ON request.user_idadmin=admin.user_id
            WHERE user.office_id IN (
                SELECT office.office_id
                FROM `office`
                LEFT JOIN `user` ON `user`.office_id=office.office_id
                WHERE user.`user_id`= '.$userId.'
            )
        ';
        $resultSet = $dbAdapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        return $resultSet;
    }

    public function getByUserId($userId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT
                request.request_id as request_id
                ,user.user_name as user_name
                ,request.request_crtdate as request_crtdate
                ,reqtype.reqtype_name as reqtype_name
                ,mtype.mtype_name as mtype_name
                ,request.request_usercom as request_usercom
                ,reqstatus.reqstatus_name as reqstatus_name
                ,admin.user_name as admin_name
                ,request.request_clsdate as request_clsdate
                ,request.request_admincom as request_admincom
                ,user_idadmin
                FROM request
                LEFT JOIN `user` ON request.user_id=user.user_id
                LEFT JOIN reqtype ON request.reqtype_id=reqtype.reqtype_id
                LEFT JOIN mtype ON request.mtype_id=mtype.mtype_id
                LEFT JOIN reqstatus ON request.reqstatus_id=reqstatus.reqstatus_id
                LEFT JOIN `user` admin ON request.user_idadmin = admin.user_id
                WHERE user.user_id = '.$userId;
        $resultSet = $dbAdapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        return $resultSet;
    }

    public function createRequest($data, $userId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        if ((int)$data['reqtype_id'] !== 3) {
            $data['mtype_id'] = 'NULL';
        }
        $sql = "INSERT INTO `accequip`.`request` (`user_id`, `reqtype_id`, `mtype_id`, `request_usercom`) VALUES
            (".(int)$userId.", ".$data['reqtype_id'].", ".$data['mtype_id'].", ".$p->quoteValue($data['request_usercom']).")";
        $resultSet = $dbAdapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        return $resultSet;
    }

}