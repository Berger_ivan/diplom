<?php
namespace Application\Model\Entity;

class User
{
    /**
     * @var int
     */
    protected $user_id;
    /**
     * @var string
     */
    protected $user_name;
    /**
     * @var string
     */
    protected $user_dol;
    /**
     * @var int
     */
    protected $office_id;
    /**
     * @var int
     */
    protected $department_id;
    /**
     * @var string
     */
    protected $user_mail;
    /**
     * @var string
     */
    protected $user_pas;
    /**
     * @var boolean
     */
    protected $user_flag;
    /**
     * @var boolean
     */
    protected $user_deleted;

    /**
     * @return int
     */
    public function getDepartmentId()
    {
        return $this->department_id;
    }

    /**
     * @param int $department_id
     */
    public function setDepartmentId($department_id)
    {
        $this->department_id = $department_id;
    }

    /**
     * @return int
     */
    public function getOfficeId()
    {
        return $this->office_id;
    }

    /**
     * @param int $office_id
     */
    public function setOfficeId($office_id)
    {
        $this->office_id = $office_id;
    }

    /**
     * @return boolean
     */
    public function isUserDeleted()
    {
        return $this->user_deleted;
    }

    /**
     * @param boolean $user_deleted
     */
    public function setUserDeleted($user_deleted)
    {
        $this->user_deleted = $user_deleted;
    }



    /**
     * @return boolean
     */
    public function isUserFlag()
    {
        return $this->user_flag;
    }

    /**
     * @param boolean $user_flag
     */
    public function setUserFlag($user_flag)
    {
        $this->user_flag = $user_flag;
    }

    /**
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->isUserFlag();
    }

    /**
     * @return string
     */
    public function getUserDol()
    {
        return $this->user_dol;
    }

    /**
     * @param string $user_dol
     */
    public function setUserDol($user_dol)
    {
        $this->user_dol = $user_dol;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getUserMail()
    {
        return $this->user_mail;
    }

    /**
     * @param string $user_mail
     */
    public function setUserMail($user_mail)
    {
        $this->user_mail = $user_mail;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->user_name;
    }

    /**
     * @param string $user_name
     */
    public function setUserName($user_name)
    {
        $this->user_name = $user_name;
    }

    /**
     * @return string
     */
    public function getUserPas()
    {
        return $this->user_pas;
    }

    /**
     * @param string $user_pas
     */
    public function setUserPas($user_pas)
    {
        $this->user_pas = $user_pas;
    }






    public function exchangeArray($data)
    {
        $this->user_id = isset($data['user_id']) ? (int)$data['user_id'] : null;
        $this->user_name = isset($data['user_name']) ? $data['user_name'] : null;
        $this->user_dol = isset($data['user_dol']) ? $data['user_dol'] : null;
        $this->office_id = isset($data['office_id']) ? (int)$data['office_id'] : null;
        $this->department_id = isset($data['department_id']) ? (int)$data['department_id'] : null;
        $this->user_mail = isset($data['user_mail']) ? $data['user_mail'] : null;
        $this->user_pas = isset($data['user_pas']) ? $data['user_pas'] : null;
        $this->user_flag = isset($data['user_flag']) ? (int)(boolean)$data['user_flag'] : null;
        $this->user_deleted = isset($data['user_deleted']) ? (int)(boolean)$data['user_deleted'] : null;
        return $this;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}