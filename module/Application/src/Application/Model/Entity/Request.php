<?php
namespace Application\Model\Entity;

class Request
{

    /**
     * @var int
     */
    protected $request_id;
    /**
     * @var int
     */
    protected $user_id;
    /**
     * @var string
     */
    protected $request_crtdate;
    /**
     * @var int
     */
    protected $reqtype_id;
    /**
     * @var int
     */
    protected $mtype_id;
    /**
     * @var string
     */
    protected $request_usercom;
    /**
     * @var int
     */
    protected $reqstatus_id;
    /**
     * @var int
     */
    protected $user_idadmin;
    /**
     * @var string
     */
    protected $request_clsdate;
    /**
     * @var string
     */
    protected $request_admincom;

    /**
     * @return int
     */
    public function getMtypeId()
    {
        return $this->mtype_id;
    }

    /**
     * @param int $mtype_id
     */
    public function setMtypeId($mtype_id)
    {
        $this->mtype_id = $mtype_id;
    }

    /**
     * @return int
     */
    public function getReqstatusId()
    {
        return $this->reqstatus_id;
    }

    /**
     * @param int $reqstatus_id
     */
    public function setReqstatusId($reqstatus_id)
    {
        $this->reqstatus_id = $reqstatus_id;
    }

    /**
     * @return int
     */
    public function getReqtypeId()
    {
        return $this->reqtype_id;
    }

    /**
     * @param int $reqtype_id
     */
    public function setReqtypeId($reqtype_id)
    {
        $this->reqtype_id = $reqtype_id;
    }

    /**
     * @return string
     */
    public function getRequestAdmincom()
    {
        return $this->request_admincom;
    }

    /**
     * @param string $request_admincom
     */
    public function setRequestAdmincom($request_admincom)
    {
        $this->request_admincom = $request_admincom;
    }

    /**
     * @return string
     */
    public function getRequestClsdate()
    {
        return $this->request_clsdate;
    }

    /**
     * @param string $request_clsdate
     */
    public function setRequestClsdate($request_clsdate)
    {
        $this->request_clsdate = $request_clsdate;
    }

    /**
     * @return string
     */
    public function getRequestCrtdate()
    {
        return $this->request_crtdate;
    }

    /**
     * @param string $request_crtdate
     */
    public function setRequestCrtdate($request_crtdate)
    {
        $this->request_crtdate = $request_crtdate;
    }

    /**
     * @return int
     */
    public function getRequestId()
    {
        return $this->request_id;
    }

    /**
     * @param int $request_id
     */
    public function setRequestId($request_id)
    {
        $this->request_id = $request_id;
    }

    /**
     * @return string
     */
    public function getRequestUsercom()
    {
        return $this->request_usercom;
    }

    /**
     * @param string $request_usercom
     */
    public function setRequestUsercom($request_usercom)
    {
        $this->request_usercom = $request_usercom;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getUserIdadmin()
    {
        return $this->user_idadmin;
    }

    /**
     * @param int $user_idadmin
     */
    public function setUserIdadmin($user_idadmin)
    {
        $this->user_idadmin = $user_idadmin;
    }



    public function exchangeArray($data)
    {
        $this->request_id = isset($data['request_id']) ? $data['request_id'] : null;
        $this->user_id = isset($data['user_id']) ? $data['user_id'] : null;
        $this->request_crtdate = isset($data['request_crtdate']) ? $data['request_crtdate'] : null;
        $this->reqtype_id = isset($data['reqtype_id']) ? $data['reqtype_id'] : null;
        $this->mtype_id = isset($data['mtype_id']) ? $data['mtype_id'] : null;
        $this->request_usercom = isset($data['request_usercom']) ? $data['request_usercom'] : null;
        $this->reqstatus_id = isset($data['reqstatus_id']) ? $data['reqstatus_id'] : null;
        $this->user_idadmin = isset($data['user_idadmin']) ? $data['user_idadmin'] : null;
        $this->request_clsdate = isset($data['request_clsdate']) ? $data['request_clsdate'] : null;
        $this->request_admincom = isset($data['request_admincom']) ? $data['request_admincom'] : null;
        return $this;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}