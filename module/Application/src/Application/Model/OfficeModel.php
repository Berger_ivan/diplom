<?php

namespace Application\Model;

use Application\Model\Table;

class OfficeModel
{

    /**
     * @return array
     */
    public function getOfficesForSelect()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT office.office_id'
            .', CONCAT(office.office_name,", ",office.office_town,", ",office.office_adr) as office_name'
            .' FROM office ORDER BY office.office_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['office_id']] = $res['office_name'];
        }
        return $selectData;
    }

    public function getOfficeById($id)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = " SELECT * FROM `office` WHERE office_id = " . (int)$id;
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();
    }

    public function getAllOffices()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT * FROM office ORDER BY office.office_id';
        return $dbAdapter->query($sql)->execute();
    }

    public function updateOffice($officeId, $officeName, $officeTown, $officeAdr)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            UPDATE `office`
            SET
            `office`.`office_name` = ".$dbAdapter->getPlatform()->quoteValue($officeName)."
            ,`office`.`office_town` = ".$dbAdapter->getPlatform()->quoteValue($officeTown)."
            ,`office`.`office_adr` = ".$dbAdapter->getPlatform()->quoteValue($officeAdr)."
            WHERE `office_id` = ".(int)$officeId;

        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

    public function createOffice($officeName, $officeTown, $officeAdr)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "INSERT INTO `office` (`office_name`,`office_town`,`office_adr`) VALUES
        (".$p->quoteValue($officeName).", ".$p->quoteValue($officeTown).", ".$p->quoteValue($officeAdr).")";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

}