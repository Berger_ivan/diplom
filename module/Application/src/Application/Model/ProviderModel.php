<?php

namespace Application\Model;

use Application\Model\Table;

class ProviderModel
{

    /**
     * @return array
     */
    public function getProvidersForSelect()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = '
            SELECT
                provider.provider_id as id,
                provider.provider_name as name
            FROM provider
            ORDER BY provider.provider_name
        ';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['id']] = $res['name'];
        }
        return $selectData;
    }


    public function getProviderById($id)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = " SELECT * FROM `provider` WHERE provider_id = " . (int)$id;
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();
    }

    public function getAllProviders()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT * FROM provider ORDER BY provider.provider_id';
        return $dbAdapter->query($sql)->execute();
    }

    public function updateProvider($providerId, $providerName, $providerTown, $providerAdr)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            UPDATE `provider`
            SET
            `provider`.`provider_name` = ".$dbAdapter->getPlatform()->quoteValue($providerName)."
            ,`provider`.`provider_town` = ".$dbAdapter->getPlatform()->quoteValue($providerTown)."
            ,`provider`.`provider_adr` = ".$dbAdapter->getPlatform()->quoteValue($providerAdr)."
            WHERE `provider_id` = ".(int)$providerId;

        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

    public function createProvider($providerName, $providerTown, $providerAdr)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "INSERT INTO `provider` (`provider_name`,`provider_town`,`provider_adr`) VALUES
        (".$p->quoteValue($providerName).", ".$p->quoteValue($providerTown).", ".$p->quoteValue($providerAdr).")";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }


}