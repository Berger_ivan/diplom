<?php

namespace Application\Model;

class AuthStorage extends \Zend\Authentication\Storage\Session
{

    public function setRememberMe($rememberMe = false, $time = 86400)
    {
        if ($rememberMe === true) {
            $this->session->getManager()->rememberMe($time);
        }
    }

    public function forgetMe()
    {
        $this->session->getManager()->forgetMe();
    }

}