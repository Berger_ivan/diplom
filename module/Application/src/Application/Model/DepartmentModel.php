<?php

namespace Application\Model;

use Application\Model\Table;

class DepartmentModel
{
    public function getAllDepartments()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT * FROM department ORDER BY department.department_id';
        return $dbAdapter->query($sql)->execute();
    }

    public function getDepartmentsByOfficeIdForEquipment($oldOfficeId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = '
            SELECT DISTINCT department.department_id, department.department_name
            FROM department
            LEFT JOIN equipment ON equipment.department_id = department.department_id
            WHERE equipment.office_id = '.(int)$oldOfficeId;
        $result = $dbAdapter->query($sql)->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['department_id']] = $res['department_name'];
        }
        return $selectData;
    }

    public function getDepartmentsByOfficeIdForMaterial($oldOfficeId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = '
            SELECT DISTINCT department.department_id, department.department_name
            FROM department
            LEFT JOIN material ON material.department_id = department.department_id
            WHERE material.office_id = '.(int)$oldOfficeId;
        $result = $dbAdapter->query($sql)->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['department_id']] = $res['department_name'];
        }
        return $selectData;
    }

    /**
     * @return array
     */
    public function getDepartmentsForSelect()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT department.department_id, department.department_name'
            .' FROM department ORDER BY department.department_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['department_id']] = $res['department_name'];
        }
        return $selectData;
    }

    public function getDepartmentById($id)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = " SELECT * FROM `department` WHERE department_id = " . (int)$id;
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();
    }

    public function updateDepartment($departmentId, $departmentName)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            UPDATE `department`
            SET `department`.`department_name` = ".$dbAdapter->getPlatform()->quoteValue($departmentName)."
            WHERE `department_id` = ".(int)$departmentId;

        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

    public function createDepartment($departmentName)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "INSERT INTO `department` (`department_name`) VALUES (".$p->quoteValue($departmentName).")";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->count() > 0;
    }

}