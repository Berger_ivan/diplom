<?php

namespace Application\Model;

use Application\Model\Table;
use Zend\Validator\Date;

class MaterialModel
{

    /**
     * @return array
     */
    public function getMaterialsForSelect()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT
            material.material_id,
            material.material_name,
            office.office_id,
            office.office_name,
            department.department_id,
            department.department_name
            FROM material
            LEFT JOIN office ON office.office_id = material.office_id
            LEFT JOIN department ON department.department_id = material.department_id
            ORDER BY material.material_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        return $result;
    }

    public function getMaterialsByOfficeIdAndDepartmentId($oldOfficeId, $oldDepartmentId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT
            material.material_id,
            material.material_name,
            material.material_count
            FROM material
            LEFT JOIN office ON office.office_id = material.office_id
            LEFT JOIN department ON department.department_id = material.department_id
            WHERE material.office_id = '.(int)$oldOfficeId.' AND material.department_id = '.(int)$oldDepartmentId.'
            ORDER BY material.material_name';
        $result = $dbAdapter->query($sql)->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['material_id']] = array('name'=>$res['material_name'], 'count'=>$res['material_count']);
        }
        return $selectData;

    }

    /**
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function getAll()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            SELECT
                material.material_id,
                material.material_name,
                mtype.mtype_name,
                material.material_count,
                material.material_name,
                office.office_name,
                department.department_name,
                provider.provider_name
            FROM material
            LEFT JOIN mtype ON material.mtype_id = mtype.mtype_id
            LEFT JOIN office ON material.office_id = office.office_id
            LEFT JOIN department ON material.department_id = department.department_id
            LEFT JOIN provider ON material.provider_id = provider.provider_id
        ";
        $result = $dbAdapter->query($sql)->execute();
        return $result;
    }

    public function getById($id)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = " SELECT * FROM `material` WHERE material_id = " . (int)$id;
        $result = $dbAdapter->query($sql)->execute();
        return $result->count() == 0 ? null : $result->current();
    }

    public function createMaterial($data)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "
            INSERT INTO `material` (
            material_name,
            mtype_id,
            material_count,
            office_id,
            department_id,
            provider_id
            ) VALUES (
            ".$p->quoteValue($data['material_name']).",
            ".(int)$data['mtype_id'].",
            ".(int)$data['material_count'].",
            ".(int)$data['office_id'].",
            ".(int)$data['department_id'].",
            ".(int)$data['provider_id']."
            )
        ";
        $result = $dbAdapter->query($sql)->execute();
        return $result->getGeneratedValue();
    }

    public function updateMaterial($id, $data)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $p = $dbAdapter->getPlatform();
        $sql = "UPDATE `material` SET ";
        $sqlParams = array();
        if (!empty($data['material_name'])) $sqlParams[] = " `material_name` = ".$p->quoteValue($data['material_name']);
        if (!empty($data['mtype_id'])) $sqlParams[] = " `mtype_id` = ".(int)$data['mtype_id'];
        if (!empty($data['material_count'])) $sqlParams[] = " `material_count` = ".(int)$data['material_count'];
        if (!empty($data['office_id'])) $sqlParams[] = " `office_id` = ".(int)$data['office_id'];
        if (!empty($data['departament_id'])) $sqlParams[] = " `departament_id` = ".(int)$data['departament_id'];
        if (!empty($data['provider_id'])) $sqlParams[] = " `provider_id` = ".(int)$data['provider_id'];

        $sql .= implode(', ', $sqlParams);
        $sql .= " WHERE `material_id` = ".(int)$id;
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

//    public function getRepair()
//    {
//        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
//        $sql = "
//            SELECT
//            `repair_id`,
//            `repair_startdate`,
//            `repair_enddate`,
//            `equipment`.`equipment_name`
//            FROM `repair`
//            LEFT JOIN `equipment` ON `equipment`.`equipment_id`=`repair`.`equipment_id`
//        ";
//        $resultSet = $dbAdapter->query($sql)->execute();
//        return $resultSet;
//    }
//
//    public function getRemote()
//    {
//        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
//        $sql = "
//            SELECT
//            `eqremote_id`,
//            `eqremote_date`,
//            `equipment`.`equipment_name`
//            FROM `eqremote`
//            LEFT JOIN `equipment` ON `equipment`.`equipment_id`=`eqremote`.`equipment_id`
//        ";
//        $resultSet = $dbAdapter->query($sql)->execute();
//        return $resultSet;
//    }

    public function getPlaning(\DateTime $startDate = null, \DateTime $endDate = null)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            SELECT office.office_name,mtype_name, CEIL(COUNT(request.mtype_id)*1.05) AS col FROM request
            INNER JOIN  mtype ON request.mtype_id=mtype.mtype_id
            INNER JOIN `user` ON `user`.user_id=request.user_id
            INNER JOIN office ON office.office_id=user.office_id
            WHERE reqtype_id=3
        ";

        if ($startDate) {
            $startDate = $startDate->modify('-1 month');
            $sql .= "AND request.request_crtdate >= '".$startDate->format('Y-m-d 00:00:00')."'";
        }
        if ($endDate) {
            $endDate = $endDate->modify('-1 month');
            $sql .= "AND request.request_crtdate <= '".$endDate->format('Y-m-d 23:59:59')."'";
        }

        $sql .= "GROUP BY office.office_name,mtype_name";

        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

    public function getMovementHistory()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            SELECT
            `mmove`.`mmove_id`,
            `material`.`material_name`,
            `mmove`.`mmove_date`,
            `mmove`.`mmove_count`,
            `office1`.`office_name` as `office_name_old`,
            `office2`.`office_name` as `office_name_new`,
            `department1`.`department_name` as `department_name_old`,
            `department2`.`department_name` as `department_name_new`
            FROM `mmove`
            LEFT JOIN `material` ON `material`.`material_id`=`mmove`.`material_id`
            LEFT JOIN `office` as `office1` ON `office1`.`office_id`=`mmove`.`mmove_officeold`
            LEFT JOIN `office` as `office2` ON `office2`.`office_id`=`mmove`.`mmove_officenew`
            LEFT JOIN `department` as `department1` ON `department1`.`department_id`=`mmove`.`mmove_departmentold`
            LEFT JOIN `department` as `department2` ON `department2`.`department_id`=`mmove`.`mmove_departmentnew`
        ";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet;
    }

    public function moveMaterial($materialId, $moveCount, $officeId, $departmentId)
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = "
            INSERT INTO `mmove`
            (`material_id`, `mmove_count`, `mmove_officeold`, `mmove_departmentold`, `mmove_officenew`, `mmove_departmentnew`)
            VALUES
            (".(int)$materialId.",".(int)$moveCount.",
             (SELECT office_id FROM material WHERE material.material_id = ".(int)$materialId."),
             (SELECT department_id FROM material WHERE material.material_id = ".(int)$materialId."),
             ".(int)$officeId.", ".(int)$departmentId."
            )
        ";
        $resultSet = $dbAdapter->query($sql)->execute();

        $sql = "
            INSERT INTO `material`
            (`material_name`, `mtype_id`, `material_count`, `office_id`, `department_id`, `provider_id`)
            VALUES
            (
             (SELECT material_name FROM material AS m1 WHERE m1.material_id = ".(int)$materialId."),
             (SELECT mtype_id FROM material AS m2 WHERE m2.material_id = ".(int)$materialId."),
             '".(int)$moveCount."',
             '".(int)$officeId."',
             '".(int)$departmentId."',
             (SELECT provider_id FROM material AS m5 WHERE m5.material_id = ".(int)$materialId.")
            )
        ";
        $resultSet = $dbAdapter->query($sql)->execute();

        $sql = "
            UPDATE `material` SET `material_count` = (`material_count` - ".(int)$moveCount.") WHERE material_id = ".(int)$materialId."
        ";
        $resultSet = $dbAdapter->query($sql)->execute();
        return $resultSet->getAffectedRows();
    }

    public function getMaterialTypes()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT `mtype`.`mtype_id` as id, `mtype`.`mtype_name` as name FROM `mtype`';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['id']] = $res['name'];
        }
        return $selectData;
    }

}