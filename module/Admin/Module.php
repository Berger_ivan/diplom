<?php

namespace Admin;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
//        $translator = $e->getApplication()->getServiceManager()->get('translator');
//        $translator
//            ->setLocale(\Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']))
//            ->setFallbackLocale('ru_RU');

        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $e->getApplication()->getServiceManager()->get('Zend\Db\Adapter\Adapter');

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, function (MvcEvent $e) {

        });
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories'=>array(
                'AuthStorage'=>function($serviceManager){
                    return new \Application\Model\AuthStorage('diplom');
                },
                'AuthService'=>function($serviceManager){
                    $dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter(
                        $dbAdapter, 'users', 'login', 'password', '?'
                    );
                    $authService = new \Zend\Authentication\AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($serviceManager->get('AuthStorage'));
                    return $authService;
                },
            ),
        );
    }

}