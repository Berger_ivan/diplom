<?php

namespace Admin\Controller;

use Admin\Form\CreateEditMaterialForm;
use Admin\Form\MaterialMovementForm;
use Admin\Form\MaterialPlaningForm;
use Application\Model\DepartmentModel;
use Application\Model\MaterialModel;
use Zend\View\Model\ViewModel;

class MaterialController extends \Application\Controller\AbstractController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();

//        var_dump($this->getAuthService()->getStorage()->read());

        $materialModel = new MaterialModel();

        $viewModel->setVariable('materials', $materialModel->getAll());
        return $viewModel;
    }

    public function createAction()
    {
        $viewModel = new ViewModel();

        $form = new CreateEditMaterialForm();
        $form->get('submit')->setValue('Создать');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $materialModel = new \Application\Model\MaterialModel();
                $result = $materialModel->createMaterial($form->getData());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'material'));
                }
            }
        }

        $viewModel->setVariable('form', $form);
        return $viewModel;
    }

    public function editAction()
    {
        $viewModel = new ViewModel();

        $materialId = $this->params()->fromQuery('id', null);
        $materialModel = new \Application\Model\MaterialModel();
        $material = $materialModel->getById($materialId);
        if (!$materialId || !$material) {
            $this->flashMessenger()->addMessage('ID не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'material'));
        }

        $form = new CreateEditMaterialForm();
        $form->get('submit')->setValue('Сохранить');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $result = $materialModel->updateMaterial($materialId, $form->getData());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'material'));
                }
            }
        } else {
            $form->setData($material);
            $form->isValid();
        }


        $viewModel->setVariable('form', $form);

        return $viewModel;
    }

//    public function repairAction()
//    {
//        $viewModel = new ViewModel();
//
//        $equipmentsModel = new EquipmentModel();
//
//
//        $equipments = $equipmentsModel->getRepair();
//        $viewModel->setVariable('equipments', $equipments);
//
//        return $viewModel;
//    }
//
//    public function remoteAction()
//    {
//        $viewModel = new ViewModel();
//
//        $equipmentsModel = new EquipmentModel();
//
//
//        $equipments = $equipmentsModel->getRemote();
//        $viewModel->setVariable('equipments', $equipments);
//
//        return $viewModel;
//    }
//
    public function movementHistoryAction()
    {
        $viewModel = new ViewModel();

        $materialModel = new MaterialModel();


        $materials = $materialModel->getMovementHistory();
        $viewModel->setVariable('materials', $materials);

        return $viewModel;
    }

    public function movementAction()
    {
        $viewModel = new ViewModel();
        $request = $this->getRequest();

        $viewModel->setTerminal($request->isXmlHttpRequest());
        if ($request->isXmlHttpRequest()) {
            $data = array();

            $html = null;

            if ($oldOfficeId = $request->getPost('old_office_id', null)) {
                if ($oldDepartmentId = $request->getPost('old_department_id', null)) {
                    $model = new MaterialModel();
                    $data = $model->getMaterialsByOfficeIdAndDepartmentId($oldOfficeId, $oldDepartmentId);
                } else {
                    $model = new DepartmentModel();
                    $data = $model->getDepartmentsByOfficeIdForMaterial($oldOfficeId);
                }
            } else if ($officeId = $request->getPost('office_id', null)) {
                $model = new DepartmentModel();
                $data = $model->getDepartmentsByOfficeIdForMaterial($officeId);
            }
            if (!empty($data)) {
                $html = '<option>...</option>';
                foreach ($data as $key=>$val) {
                    if (is_array($val)) {
                        $name = '';
                        $attrs = '';
                        foreach ($val as $k=>$v) {
                            if ($k == 'name') {
                                $name = $v;
                            } else {
                                $attrs .= ' data-'.$k.'="'.$v.'" ';
                            }
                        }
                        $html .= '<option '.$attrs.' value="'.$key.'">'.$name.'</option>';
                    } else {
                        $html .= '<option value="'.$key.'">'.$val.'</option>';
                    }
                }
            }

            $response = $this->getResponse();
//            $response->setContent(\Zend\Json\Json::encode($data));
            $response->setContent($html);
            return $response;
        }


        $form = new MaterialMovementForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $materialModel = new MaterialModel();
                $result = $materialModel->moveMaterial(
                    $form->getData()['material_id'],
                    $form->getData()['mmove_count'],
                    $form->getData()['office_id'],
                    $form->getData()['department_id']
                );
                if ($result) {
                    $viewModel->setVariable('message', 'Техника перемещена успешно');
                    return $this->redirect()->toRoute('admin', array('controller'=>'material', 'action'=>'movement-history'));
                }
            }
        }

        $viewModel->setVariable('form', $form);

        return $viewModel;
    }

    public function planingAction()
    {
        $viewModel = new ViewModel();

        $startDate = null;
        $endDate = null;

        $form = new MaterialPlaningForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
//                $startDate = \DateTime::createFromFormat($form->get('date_start')->getFormat(), $form->get('date_start')->getValue());
//                $endDate = \DateTime::createFromFormat($form->get('date_end')->getFormat(), $form->get('date_end')->getValue());
                $startDate = \DateTime::createFromFormat('Y-m-d', $form->get('date_start')->getValue());
                $endDate = \DateTime::createFromFormat('Y-m-d', $form->get('date_end')->getValue());

                $materialModel = new MaterialModel();
                $materials = $materialModel->getPlaning($startDate, $endDate);
                $viewModel->setVariable('materials', $materials);
            }
        }

        $viewModel->setVariable('form', $form);



        return $viewModel;
    }


}