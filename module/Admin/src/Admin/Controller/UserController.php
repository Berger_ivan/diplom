<?php

namespace Admin\Controller;

use Admin\Form\CreateEditUserForm;
use Zend\View\Model\ViewModel;

class UserController extends \Application\Controller\AbstractController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();

//        var_dump($this->getAuthService()->getStorage()->read());
        $usersModel = new \Application\Model\UsersModel();

        $viewModel->setVariable('users', $usersModel->getAllUsers());


        return $viewModel;
    }
    public function createAction()
    {
        $viewModel = new ViewModel();

        $form = new CreateEditUserForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $usersModel = new \Application\Model\UsersModel();
                $result = $usersModel->createUser($form->getData());
                if ($result) {
                    $message = 'Пользователь успешно создан';
                    $this->flashmessenger()->addMessage($message);
//                    $viewModel->setVariable('message', $message);
                    return $this->redirect()->toRoute('admin', array('controller'=>'user'));
                }
            }
        }

        $viewModel->setVariable('form', $form);

        return $viewModel;
    }
    public function editAction()
    {
        $viewModel = new ViewModel();

        $userId = $this->params()->fromQuery('userid', null);
        $usersModel = new \Application\Model\UsersModel();
        $user = $usersModel->getUser($userId);
        if (!$userId || !$user) {
            $this->flashmessenger()->addMessage('ID пользователя не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'user'));
        }

        $form = new CreateEditUserForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $result = $usersModel->updateUser($userId, $form->getData());
                if ($result) {
                    $message = 'Пользователь успешно изменён';
                    $this->flashmessenger()->addMessage($message);
//                    $viewModel->setVariable('message', $message);
                    return $this->redirect()->toRoute('admin', array('controller'=>'user'));
                }
            }
        } else {
            $form->setData($user);
            $form->isValid();
        }

        $viewModel->setVariable('form', $form);

        return $viewModel;
    }

//    public function matsAction()
//    {
//        $viewModel = new ViewModel();
//
//        $model = new \Admin\Model\MatsModel();
//
//        $mats = $model->getAllMats();
//        $viewModel->setVariable('mats', $mats);
//
//        return $viewModel;
//    }
//
//    public function testAction()
//    {
//        $viewModel = new ViewModel();
//        return $viewModel;
//    }

}
