<?php

namespace Admin\Controller;

use Admin\Form\CreateEditEquipmentForm;
use Admin\Form\EquipmentMovementForm;
use Application\Model\DepartmentModel;
use Application\Model\EquipmentModel;
use Application\Model\OfficeModel;
use Application\Model\UsersModel;
use Zend\View\Model\ViewModel;

class EquipmentController extends \Application\Controller\AbstractController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();

//        var_dump($this->getAuthService()->getStorage()->read());

        $equipmentsModel = new EquipmentModel();

        $viewModel->setVariable('equipments', $equipmentsModel->getAllEquipments());

        return $viewModel;
    }

    public function createAction()
    {
        $viewModel = new ViewModel();

        $form = new CreateEditEquipmentForm();
        $form->get('submit')->setValue('Создать');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $equipmentModel = new \Application\Model\EquipmentModel();
                $result = $equipmentModel->createEquipment($form->getData());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'equipment'));
                }
            }
        }

        $viewModel->setVariable('form', $form);

        return $viewModel;
    }

    public function editAction()
    {
        $viewModel = new ViewModel();

        $equipmentId = $this->params()->fromQuery('id', null);
        $equipmentModel = new \Application\Model\EquipmentModel();
        $equipment = $equipmentModel->getById($equipmentId);
        if (!$equipmentId || !$equipment) {
            $this->flashmessenger()->addMessage('ID не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'equipment'));
        }

        $repair = $equipmentModel->getRepairByEquipmentId($equipmentId);

        $form = new CreateEditEquipmentForm($equipmentId > 0);
        $form->get('submit')->setValue('Сохранить');
        $request = $this->getRequest();
        if ($request->isPost()) {

            $repair['repair_provider_id'] = $repair['provider_id'];
            unset($repair['provider_id']);

            $form->setData(array_merge($repair, $request->getPost()->toArray()));

            if (!(bool)$request->getPost('equipment_flagrepair')) {
                $form->getInputFilter()->get('repair_type')->setRequired(FALSE);
                $form->getInputFilter()->get('repair_type')->setAllowEmpty(TRUE);

                $form->getInputFilter()->get('repair_startdate')->setRequired(FALSE);
                $form->getInputFilter()->get('repair_startdate')->setAllowEmpty(TRUE);

                $form->getInputFilter()->get('repair_enddate')->setRequired(FALSE);
                $form->getInputFilter()->get('repair_enddate')->setAllowEmpty(TRUE);

                $form->getInputFilter()->get('provider_id')->setRequired(FALSE);
                $form->getInputFilter()->get('provider_id')->setAllowEmpty(TRUE);

                $form->getInputFilter()->get('repair_reason')->setRequired(FALSE);
                $form->getInputFilter()->get('repair_reason')->setAllowEmpty(TRUE);
            }

            if ($form->isValid()) {
                if ((bool)$form->get('equipment_flagremote')->getValue()) {
                    $equipmentModel->setRemote($equipmentId);
                }

                if ($form->get('equipment_flagrepair')->getValue() == 1) {
                    if ($repair && isset($repair['repair_id'])) {
                        $equipmentModel->updateRepair(
                            $repair['repair_id']
                            ,$equipmentId
                            ,$form->get('repair_type')->getValue()
                            ,$form->get('repair_startdate')->getValue()
                            ,$form->get('repair_enddate')->getValue()
                            ,$form->get('repair_provider_id')->getValue()
                            ,$form->get('repair_reason')->getValue()
                        );
                    } else {
                        $equipmentModel->createRepair(
                            $equipmentId
                            ,$form->get('repair_type')->getValue()
                            ,$form->get('repair_startdate')->getValue()
                            ,$form->get('repair_enddate')->getValue()
                            ,$form->get('repair_provider_id')->getValue()
                            ,$form->get('repair_reason')->getValue()
                        );
                    }

                }

                $result = $equipmentModel->updateEquipment($equipmentId, $form->getData());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'equipment'));
                }
            }
        } else {
            if ($repair) {
                $equipment = array_merge($repair, $equipment);
            }
            $form->setData($equipment);
        }

        $viewModel->setVariable('form', $form);

        return $viewModel;
    }

    public function repairAction()
    {
        $viewModel = new ViewModel();

        $equipmentsModel = new EquipmentModel();


        $equipments = $equipmentsModel->getRepair();
        $viewModel->setVariable('equipments', $equipments);

        return $viewModel;
    }

    public function remoteAction()
    {
        $viewModel = new ViewModel();

        $equipmentsModel = new EquipmentModel();


        $equipments = $equipmentsModel->getRemote();
        $viewModel->setVariable('equipments', $equipments);

        return $viewModel;
    }

    public function movementHistoryAction()
    {
        $viewModel = new ViewModel();

        $equipmentsModel = new EquipmentModel();


        $equipments = $equipmentsModel->getEquipmentMovementHistory();
        $viewModel->setVariable('equipments', $equipments);

        return $viewModel;
    }

    public function movementAction()
    {
        $viewModel = new ViewModel();
        $request = $this->getRequest();

        $viewModel->setTerminal($request->isXmlHttpRequest());

        if ($request->isXmlHttpRequest()) {
            $data = array();

            $html = null;

            if ($oldOfficeId = $request->getPost('old_office_id', null)) {
                if ($oldDepartmentId = $request->getPost('old_department_id', null)) {
                    $model = new EquipmentModel();
                    $data = $model->getEquipmentsByOfficeIdAndDepartmentId($oldOfficeId, $oldDepartmentId);
                } else {
                    $model = new DepartmentModel();
                    $data = $model->getDepartmentsByOfficeIdForEquipment($oldOfficeId);
                }
            } else if ($officeId = $request->getPost('office_id', null)) {
                if ($departmentId = $request->getPost('department_id', null)) {
                    $model = new UsersModel();
                    $data = $model->getUsersByOfficeIdAndDepartmentId($officeId, $departmentId);
                } else {
                    $model = new DepartmentModel();
                    $data = $model->getDepartmentsByOfficeIdForMaterial($officeId);
                }

            }
            if (!empty($data)) {
                $html = '<option>...</option>';
                foreach ($data as $key=>$val) {
                    if (is_array($val)) {
                        $name = '';
                        $attrs = '';
                        foreach ($val as $k=>$v) {
                            if ($k == 'name') {
                                $name = $v;
                            } else {
                                $attrs .= ' data-'.$k.'="'.$v.'" ';
                            }
                        }
                        $html .= '<option '.$attrs.' value="'.$key.'">'.$name.'</option>';
                    } else {
                        $html .= '<option value="'.$key.'">'.$val.'</option>';
                    }
                }
            }

            $response = $this->getResponse();
//            $response->setContent(\Zend\Json\Json::encode($data));
            $response->setContent($html);
            return $response;
        }


        $form = new EquipmentMovementForm();
        $form->get('submit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $equipmentModel = new EquipmentModel();
                $result = $equipmentModel->moveEquipment(
                    $form->getData()['equipment_id'],
                    $form->getData()['office_id'],
                    $form->getData()['department_id'],
                    $form->getData()['new_user_id']
                );
                if ($result) {
                    $viewModel->setVariable('message', 'Техника перемещена успешно');
                    return $this->redirect()->toRoute('admin', array('controller'=>'equipment', 'action'=>'movement-history'));
                }
            }
        }

        $viewModel->setVariable('form', $form);

        return $viewModel;
    }

//    public function createRepairAction()
//    {
//        $viewModel = new ViewModel();
//
//        $form = new EquipmentCreateRepairForm();
//
////        $equipmentsModel = new EquipmentModel();
////        $equipments = $equipmentsModel->getRepair();
//
//        $viewModel->setVariable('form', $form);
//
//        return $viewModel;
//    }

}
