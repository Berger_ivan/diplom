<?php

namespace Admin\Controller;

use Admin\Form\CatalogCreateEditDepartment;
use Admin\Form\CatalogCreateEditEqtype;
use Admin\Form\CatalogCreateEditMtype;
use Admin\Form\CatalogCreateEditOffice;
use Admin\Form\CatalogCreateEditProvider;
use Application\Model\DepartmentModel;
use Application\Model\EqtypeModel;
use Application\Model\MtypeModel;
use Application\Model\OfficeModel;
use Application\Model\ProviderModel;
use Zend\View\Model\ViewModel;

class CatalogController extends \Application\Controller\AbstractController
{

    public function departmentAction()
    {
        $viewModel = new ViewModel();
        $departmentModel = new DepartmentModel();
        $viewModel->setVariable('departments', $departmentModel->getAllDepartments());
        return $viewModel;
    }
    public function departmentCreateAction()
    {
        $viewModel = new ViewModel();
        $form = new CatalogCreateEditDepartment();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $departmentModel = new DepartmentModel();
                $result = $departmentModel->createDepartment($form->get('department_name')->getValue());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'department'));
                }
            }
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }
    public function departmentEditAction()
    {
        $viewModel = new ViewModel();
        $departmentId = $this->params()->fromQuery('id', null);
        $departmentModel = new DepartmentModel();
        $department = $departmentModel->getDepartmentById($departmentId);
        if (!$department || !$departmentId) {
            $this->flashMessenger()->addMessage('ID не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'department'));
        }
        $form = new CatalogCreateEditDepartment();
        $form->get('submit')->setValue('Сохранить');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $result = $departmentModel->updateDepartment($departmentId, $form->get('department_name')->getValue());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'department'));
                }
            }
        } else {
            $form->setData($department);
            $form->isValid();
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }









    public function officeAction()
    {
        $viewModel = new ViewModel();
        $officeModel = new OfficeModel();
        $viewModel->setVariable('offices', $officeModel->getAllOffices());
        return $viewModel;
    }
    public function officeCreateAction()
    {
        $viewModel = new ViewModel();
        $form = new CatalogCreateEditOffice();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $officeModel = new OfficeModel();
                $result = $officeModel->createOffice(
                    $form->get('office_name')->getValue(),
                    $form->get('office_town')->getValue(),
                    $form->get('office_adr')->getValue()
                );
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'office'));
                }
            }
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }
    public function officeEditAction()
    {
        $viewModel = new ViewModel();
        $officeId = $this->params()->fromQuery('id', null);
        $officeModel = new OfficeModel();
        $office = $officeModel->getOfficeById($officeId);
        if (!$office || !$officeId) {
            $this->flashMessenger()->addMessage('ID не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'office'));
        }
        $form = new CatalogCreateEditOffice();
        $form->get('submit')->setValue('Сохранить');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $result = $officeModel->updateOffice(
                    $officeId,
                    $form->get('office_name')->getValue(),
                    $form->get('office_town')->getValue(),
                    $form->get('office_adr')->getValue()
                );
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'office'));
                }
            }
        } else {
            $form->setData($office);
            $form->isValid();
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }









    public function mtypeAction()
    {
        $viewModel = new ViewModel();
        $mtypeModel = new MtypeModel();
        $viewModel->setVariable('mtypes', $mtypeModel->getAllMtypes());
        return $viewModel;
    }
    public function mtypeCreateAction()
    {
        $viewModel = new ViewModel();
        $form = new CatalogCreateEditMtype();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $mtypeModel = new MtypeModel();
                $result = $mtypeModel->createMtype($form->get('mtype_name')->getValue());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'mtype'));
                }
            }
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }
    public function mtypeEditAction()
    {
        $viewModel = new ViewModel();
        $mtypeId = $this->params()->fromQuery('id', null);
        $mtypeModel = new MtypeModel();
        $mtype = $mtypeModel->getMtypeById($mtypeId);
        if (!$mtype || !$mtypeId) {
            $this->flashMessenger()->addMessage('ID не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'mtype'));
        }
        $form = new CatalogCreateEditMtype();
        $form->get('submit')->setValue('Сохранить');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $result = $mtypeModel->updateMtype($mtypeId, $form->get('mtype_name')->getValue());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'mtype'));
                }
            }
        } else {
            $form->setData($mtype);
            $form->isValid();
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }









    public function providerAction()
    {
        $viewModel = new ViewModel();
        $providerModel = new ProviderModel();
        $viewModel->setVariable('providers', $providerModel->getAllProviders());
        return $viewModel;
    }
    public function providerCreateAction()
    {
        $viewModel = new ViewModel();
        $form = new CatalogCreateEditProvider();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $providerModel = new ProviderModel();
                $result = $providerModel->createProvider(
                    $form->get('provider_name')->getValue(),
                    $form->get('provider_town')->getValue(),
                    $form->get('provider_adr')->getValue()
                );
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'provider'));
                }
            }
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }
    public function providerEditAction()
    {
        $viewModel = new ViewModel();
        $providerId = $this->params()->fromQuery('id', null);
        $providerModel = new ProviderModel();
        $provider = $providerModel->getProviderById($providerId);
        if (!$provider || !$providerId) {
            $this->flashMessenger()->addMessage('ID не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'provider'));
        }
        $form = new CatalogCreateEditProvider();
        $form->get('submit')->setValue('Сохранить');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $result = $providerModel->updateProvider(
                    $providerId,
                    $form->get('provider_name')->getValue(),
                    $form->get('provider_town')->getValue(),
                    $form->get('provider_adr')->getValue()
                );
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'provider'));
                }
            }
        } else {
            $form->setData($provider);
            $form->isValid();
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }






    public function eqtypeAction()
    {
        $viewModel = new ViewModel();
        $eqtypeModel = new EqtypeModel();
        $viewModel->setVariable('eqtypes', $eqtypeModel->getAllEqtypes());
        return $viewModel;
    }
    public function eqtypeCreateAction()
    {
        $viewModel = new ViewModel();
        $form = new CatalogCreateEditEqtype();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $eqtypeModel = new EqtypeModel();
                $result = $eqtypeModel->createEqtype($form->get('eqtype_name')->getValue());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'eqtype'));
                }
            }
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }
    public function eqtypeEditAction()
    {
        $viewModel = new ViewModel();
        $eqtypeId = $this->params()->fromQuery('id', null);
        $eqtypeModel = new EqtypeModel();
        $eqtype = $eqtypeModel->getEqtypeById($eqtypeId);
        if (!$eqtype || !$eqtypeId) {
            $this->flashMessenger()->addMessage('ID не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'eqtype'));
        }
        $form = new CatalogCreateEditEqtype();
        $form->get('submit')->setValue('Сохранить');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $result = $eqtypeModel->updateEqtype($eqtypeId, $form->get('eqtype_name')->getValue());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'catalog', 'action'=>'eqtype'));
                }
            }
        } else {
            $form->setData($eqtype);
            $form->isValid();
        }
        $viewModel->setVariable('form', $form);
        return $viewModel;
    }

}