<?php

namespace Admin\Controller;

use Admin\Form\NotificationForm;
use Application\Model\NotificationModel;
use Zend\View\Model\ViewModel;


class NotificationController extends \Application\Controller\AbstractController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();

        $notificationModel = new NotificationModel();

        $viewModel->setVariable('notifications', $notificationModel->getAll());

        return $viewModel;
    }

    public function createAction()
    {
        $viewModel = new ViewModel();

        $form = new NotificationForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $admin = $this->getAuthService()->getStorage()->read();
                $notificationModel = new \Application\Model\NotificationModel();

                $config = $this->getServiceLocator()->get('config');
                $fakeEmail = isset($config['email']) ? $config['email'] : null;
                $result = $notificationModel->createNotification($admin->getUserMail(), $form->getData(), $fakeEmail);
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'notification'));
                }
            }
        }

        $viewModel->setVariable('form', $form);

        return $viewModel;
    }

//    public function profileAction()
//    {
//        $viewModel = new ViewModel();
//
//        $form = new UserProfileForm();
//
//        $request = $this->getRequest();
//        if ($request->isPost()) {
//            $form->setData($request->getPost());
//            if ($form->isValid()) {
//                if ($this->getAuthService()->hasIdentity()) {
//                    $profileModel = new \Application\Model\ProfileModel();
//                    $result = $profileModel->updatePassword($this->getAuthService()->getStorage()->read()->getUserId(),$form->get('user_pas')->getValue());
//                    if ($result) {
//                        $message = 'Пароль успешно изменён';
//                    } else {
//                        $message = 'Новый пароль совпадает с текущим';
//                    }
//                    $viewModel->setVariable('message', $message);
//                }
//            }
//        }
//
//
//        $viewModel->setVariable('form', $form);
//
//        $usersModel = new \Application\Model\UsersModel();
//        if ($this->getAuthService()->hasIdentity()) {
//            $userObj = $this->getAuthService()->getStorage()->read();
//            $viewModel->setVariable('user', $usersModel->getUser($userObj->getUserId()));
//        }
//        return $viewModel;
//    }

}
