<?php

namespace Admin\Controller;

use Admin\Form\RequestForm;
use Zend\View\Model\ViewModel;

class RequestController extends \Application\Controller\AbstractController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();

        if ($this->getAuthService()->hasIdentity()) {
            $requestModel = new \Application\Model\RequestModel();
            $requests = $requestModel->getForAdmin($this->getAuthService()->getIdentity()->getUserId());
            $viewModel->setVariable('requests', $requests);
        }

        return $viewModel;
    }

    public function editAction()
    {
        $viewModel = new ViewModel();

        $requestId = $this->params()->fromQuery('id', null);
        $requestModel = new \Application\Model\RequestModel();
        $request = $requestModel->getById($requestId);
        if (!$requestId || !$request) {
            $this->flashmessenger()->addMessage('ID запроса не найден');
            return $this->redirect()->toRoute('admin', array('controller'=>'request'));
        }

        $form = new RequestForm();
        $form->get('submit')->setValue('Сохранить');
        if ($this->getRequest()->isPost()) {

            if ($this->getRequest()->getPost('reqtype_id', null) == 3) {
                $form->getInputFilter()->get('mtype_id')->setRequired(true);
            }

            $form->setData($this->getRequest()->getPost());
            if ($form->isValid()) {
                $result = $requestModel->updateRequest($requestId, $form->getData());
                if ($result) {
                    return $this->redirect()->toRoute('admin', array('controller'=>'request'));
                }
            }
        } else {
            $form->setData($request);
            $form->isValid();
        }

        $viewModel->setVariable('form', $form);
        return $viewModel;
    }

}
