<?php

namespace Admin\Controller;

use Application\Form\UserProfileForm;
use Application\Model\EquipmentModel;
use Zend\View\Model\ViewModel;

class IndexController extends \Application\Controller\AbstractController
{

    public function indexAction()
    {
        $viewModel = new ViewModel();

        $userId = $this->getAuthService()->getIdentity()->getUserId();
        $equipmentModel = new EquipmentModel();
        $viewModel->setVariable('equipments', $equipmentModel->getEquipmentByUser($userId));

        return $viewModel;
    }

    public function profileAction()
    {
        $viewModel = new ViewModel();

        $form = new UserProfileForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                if ($this->getAuthService()->hasIdentity()) {
                    $profileModel = new \Application\Model\ProfileModel();
                    $result = $profileModel->updatePassword($this->getAuthService()->getStorage()->read()->getUserId(),$form->get('user_pas')->getValue());
                    if ($result) {
                        $message = 'Пароль успешно изменён';
                    } else {
                        $message = 'Новый пароль совпадает с текущим';
                    }
                    $viewModel->setVariable('message', $message);
                }
            }
        }


        $viewModel->setVariable('form', $form);

        $usersModel = new \Application\Model\UsersModel();
        if ($this->getAuthService()->hasIdentity()) {
            $userObj = $this->getAuthService()->getStorage()->read();
            $viewModel->setVariable('user', $usersModel->getUser($userObj->getUserId()));
        }
        return $viewModel;
    }

}
