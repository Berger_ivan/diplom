<?php

namespace Admin\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class RequestForm extends Form
{

    public function __construct()
    {
        parent::__construct('request_form');

        $this->setAttribute('method','post');

        $element = new Select();
        $element->setLabel('Тип заявки');
        $element->setName('reqtype_id');
        $element->setAttribute('id', $element->getName());
        $element->setValueOptions($this->getRequestTypes());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Тип расходного материала');
        $element->setName('mtype_id');
        $element->setAttribute('id', $element->getName());
        $element->setValueOptions($this->getMTypes());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Статус заявки');
        $element->setName('reqstatus_id');
        $element->setAttribute('id', $element->getName());
        $element->setValueOptions($this->getRequestStatuses());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Исполнитель');
        $element->setName('user_idadmin');
        $element->setAttribute('id', $element->getName());
        $element->setValueOptions($this->getAdmins());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Textarea();
        $element->setLabel('Комментарий исполнителя');
        $element->setName('request_admincom');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new Submit();
        $element->setName('submit');
        $element->setValue('Создать');
        $this->add($element);

        $this->setInputFilter(new \Admin\Form\Filter\RequestFormFilter());
    }

    protected function getRequestTypes()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT reqtype_id,reqtype_name FROM reqtype ORDER BY reqtype_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['reqtype_id']] = $res['reqtype_name'];
        }
        return $selectData;
    }

    protected function getMTypes()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT mtype_id,mtype_name FROM mtype ORDER BY mtype_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['mtype_id']] = $res['mtype_name'];
        }
        return $selectData;
    }

    protected function getRequestStatuses()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT reqstatus.reqstatus_id, reqstatus.reqstatus_name FROM reqstatus ORDER BY reqstatus_id';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['reqstatus_id']] = $res['reqstatus_name'];
        }
        return $selectData;
    }

    protected function getAdmins()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT user.user_id, user.user_name FROM user WHERE user_flag = 1 ORDER BY user_id';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['user_id']] = $res['user_name'];
        }
        return $selectData;
    }

}