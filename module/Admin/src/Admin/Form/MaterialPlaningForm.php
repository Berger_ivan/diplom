<?php

namespace Admin\Form;

use Zend\Form\Element\Date;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class MaterialPlaningForm extends Form
{

    public function __construct()
    {
        parent::__construct('material_planing_form');

        $this->setAttribute('method','post');
        $this->setLabel('Период');

        $element = new Text();
        $element->setLabel('c:');
        $element->setName('date_start');
        $element->setAttribute('id',$element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new Text();
        $element->setLabel('по:');
        $element->setName('date_end');
        $element->setAttribute('id',$element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new Submit();
        $element->setName('submit');
        $element->setValue('Показать');
        $this->add($element);

        $this->setInputFilter(new \Admin\Form\Filter\MaterialPlaningFormFilter());
    }

}