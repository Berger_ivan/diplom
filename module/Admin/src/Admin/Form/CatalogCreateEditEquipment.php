<?php

namespace Admin\Form;

class CatalogCreateEditEquipment extends \Zend\Form\Form
{

    public function __construct()
    {
        parent::__construct('catalog_create_edit_equipment');

        $this->setAttribute('method', 'post');

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Наименование');
        $element->setName('equipment_name');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Инв. номер');
        $element->setName('equipment_invnom');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Select();
        $element->setLabel('Тип');
        $element->setName('eqtype_id');
        $element->setValueOptions($this->getEquipmentTypes());
        $element->setEmptyOption('...');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Textarea();
        $element->setLabel('Характеристики');
        $element->setName('equipment_characteristics');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Дата поступления');
        $element->setName('equipment_crtdate');
        $element->setAttribute('id', $element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Дата окончания гарантийного периода');
        $element->setName('equipment_warrdate');
        $element->setAttribute('id', $element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Дата окончания срока службы');
        $element->setName('equipment_life');
        $element->setAttribute('id', $element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new \Zend\Form\Element\Select();
        $element->setLabel('ID отделения');
        $element->setName('office_id');
        $element->setValueOptions($this->getOffices());
        $element->setEmptyOption('...');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Select();
        $element->setLabel('ID департамента/отдела');
        $element->setName('department_id');
        $element->setValueOptions($this->getDepartments());
        $element->setEmptyOption('...');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Select();
        $element->setLabel('ID пользователя');
        $element->setName('user_id');
        $element->setValueOptions($this->getUsers());
        $element->setEmptyOption('...');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Select();
        $element->setLabel('ID поставщика');
        $element->setName('provider_id');
        $element->setValueOptions($this->getProviders());
        $element->setEmptyOption('...');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Стоимость');
        $element->setName('equipment_cost');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Checkbox();
        $element->setLabel('Cписано');
        $element->setName('equipment_flagremote');
        $element->useHiddenElement();
        $element->setCheckedValue(1);
        $element->setUncheckedValue(0);
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Checkbox();
        $element->setLabel('В ремонте');
        $element->setName('equipment_flagrepair');
        $element->useHiddenElement();
        $element->setCheckedValue(1);
        $element->setUncheckedValue(0);
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $submit = new \Zend\Form\Element\Submit();
        $submit->setValue('Создать');
        $submit->setName('submit');
        $this->add($submit);

        $this->setInputFilter(new \Admin\Form\Filter\CreateEditEquipmentFormFilter());
    }

    protected function getEquipmentTypes()
    {
        $model = new \Application\Model\EquipmentModel();
        return $model->getEquipmentTypesForSelect();
    }

    protected function getEquipments()
    {
        $model = new \Application\Model\EquipmentModel();
        $data = array();
        foreach ($model->getEquipmentsForSelect() as $item) {
            $data[] = array(
                'value'=>$item['equipment_id'],
                'label'=>$item['equipment_name'],
                'attributes'=>array(
                    'data-equipment_id'=>$item['equipment_id'],
                    'data-equipment_name'=>$item['equipment_name'],
                    'data-office_id'=>$item['office_id'],
                    'data-office_name'=>$item['office_name'],
                    'data-department_id'=>$item['department_id'],
                    'data-department_name'=>$item['department_name'],
                )
            );
        }
        return $data;
    }

    protected function getDepartments()
    {
        $model = new \Application\Model\DepartmentModel();
        return $model->getDepartmentsForSelect();
    }

    protected function getOffices()
    {
        $model = new \Application\Model\OfficeModel();
        return $model->getOfficesForSelect();
    }

    protected function getUsers()
    {
        $model = new \Application\Model\UsersModel();
        return $model->getUsersForSelect();
    }

    protected function getProviders()
    {
        $model = new \Application\Model\ProviderModel();
        return $model->getProvidersForSelect();
    }

}



//equipment_id
//ID оборудования/ПО
//
//equipment_name
//Наименование оборудования
//
//equipment_invnom
//Инв. номер оборудования
//
//eqtype_id
//Тип оборудования
//
//equipment_characteristics
//Характеристики оборудования
//
//equipment_crtdate
//Дата поступления оборудования
//
//equipment_warrdate
//Дата окончания гарантийного периода
//
//equipment_life
//Дата окончания срока службы
//
//office_id
//ID отделения
//
//department_id
//ID департамента/отдела
//
//user_id
//ID пользователя
//
//provider_id
//ID поставщика
//
//equipment_cost
//Стоимость оборудования
//
//equipment_flagremote
//1-списано
//
//equipment_flagrepair
//1-в ремонте