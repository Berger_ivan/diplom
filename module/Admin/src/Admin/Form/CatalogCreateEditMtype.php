<?php

namespace Admin\Form;

class CatalogCreateEditMtype extends \Zend\Form\Form
{

    public function __construct()
    {
        parent::__construct('catalog_create_edit_mtype');

        $this->setAttribute('method', 'post');

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Наименование');
        $element->setName('mtype_name');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $submit = new \Zend\Form\Element\Submit();
        $submit->setValue('Создать');
        $submit->setName('submit');
        $this->add($submit);

        $this->setInputFilter(new \Admin\Form\Filter\CreateEditMtypeFormFilter());
    }

}