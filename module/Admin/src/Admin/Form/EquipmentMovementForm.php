<?php

namespace Admin\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\Mvc\Application;

class EquipmentMovementForm extends Form
{

    public function __construct()
    {
        parent::__construct('movement_form');

        $this->setAttribute('method','post');

        $element = new Select();
        $element->setLabel('Отделение');
        $element->setName('old_office_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getOffices());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Департамент');
        $element->setName('old_department_id');
        $element->setAttribute('id',$element->getName());
        $element->setAttribute('disabled','disabled');
        $element->setDisableInArrayValidator(true);
        $this->add($element);

        $element = new Select();
        $element->setLabel('Наименование');
        $element->setName('equipment_id');
        $element->setAttribute('id',$element->getName());
        $element->setAttribute('disabled','disabled');
        $element->setDisableInArrayValidator(true);
        $this->add($element);


        $element = new Select();
        $element->setLabel('Отделение');
        $element->setName('office_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getOffices());
        $element->setEmptyOption('...');
//        $element->setAttribute('disabled','disabled');
        $element->setDisableInArrayValidator(true);
        $this->add($element);

        $element = new Select();
        $element->setLabel('Департамент');
        $element->setName('department_id');
        $element->setAttribute('id',$element->getName());
        $element->setAttribute('disabled','disabled');
        $element->setDisableInArrayValidator(true);
        $this->add($element);

        $element = new Select();
        $element->setLabel('Пользователь');
        $element->setName('new_user_id');
        $element->setAttribute('id',$element->getName());
        $element->setAttribute('disabled','disabled');
        $element->setDisableInArrayValidator(true);
        $this->add($element);

        $element = new Submit();
        $element->setName('submit');
        $element->setValue('Переместить');
//        $element->setAttribute('disabled','disabled');
        $this->add($element);

        $this->setInputFilter(new \Admin\Form\Filter\EquipmentMovementFormFilter());
    }


    protected function getDepartments()
    {
        $model = new \Application\Model\DepartmentModel();
        return $model->getDepartmentsForSelect();
    }

    protected function getOffices()
    {
        $model = new \Application\Model\OfficeModel();
        return $model->getOfficesForSelect();
    }

    protected function getUsers()
    {
        $model = new \Application\Model\UsersModel();
        return $model->getUsersForSelect();
    }

    protected function getEquipments()
    {
        $model = new \Application\Model\EquipmentModel();
        $data = array();
        foreach ($model->getEquipmentsForSelect() as $item) {
            $data[] = array(
                'value'=>$item['equipment_id'],
                'label'=>$item['equipment_name'],
                'attributes'=>array(
                    'data-equipment_id'=>$item['equipment_id'],
                    'data-equipment_name'=>$item['equipment_name'],
                    'data-office_id'=>$item['office_id'],
                    'data-office_name'=>$item['office_name'],
                    'data-department_id'=>$item['department_id'],
                    'data-department_name'=>$item['department_name'],
                )
            );
        }
        return $data;
    }

}