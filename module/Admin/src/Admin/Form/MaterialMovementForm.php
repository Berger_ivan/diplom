<?php

namespace Admin\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;
use Zend\Mvc\Application;

class MaterialMovementForm extends Form
{

    public function __construct()
    {
        parent::__construct('material_movement_form');

        $this->setAttribute('method','post');

        $element = new Select();
        $element->setLabel('Отделение');
        $element->setName('old_office_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getOffices());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Департамент');
        $element->setName('old_department_id');
        $element->setAttribute('id',$element->getName());
        $element->setAttribute('disabled','disabled');
        $element->setDisableInArrayValidator(true);
        $this->add($element);




        $element = new Select();
        $element->setLabel('Наименование');
        $element->setName('material_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getMaterials());
        $element->setAttribute('disabled','disabled');
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Text();
        $element->setLabel('Количество');
        $element->setName('mmove_count');
        $element->setAttribute('id',$element->getName());
        $element->setAttribute('disabled','disabled');
        $this->add($element);




        $element = new Select();
        $element->setLabel('Отделение');
        $element->setName('office_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getOffices());
        $element->setEmptyOption('...');
//        $element->setAttribute('disabled','disabled');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Департамент');
        $element->setName('department_id');
        $element->setAttribute('id',$element->getName());
        $element->setAttribute('disabled','disabled');
        $element->setEmptyOption('...');
        $element->setDisableInArrayValidator(true);
        $this->add($element);

        $element = new Submit();
        $element->setName('submit');
//        $element->setAttribute('disabled','disabled');
        $element->setValue('Переместить');
        $this->add($element);

        $this->setInputFilter(new \Admin\Form\Filter\MaterialMovementFormFilter());
    }


    protected function getDepartments()
    {
        $model = new \Application\Model\DepartmentModel();
        return $model->getDepartmentsForSelect();
    }

    protected function getOffices()
    {
        $model = new \Application\Model\OfficeModel();
        return $model->getOfficesForSelect();
    }

    protected function getMaterials()
    {
        $model = new \Application\Model\MaterialModel();
        $data = array();
        foreach ($model->getMaterialsForSelect() as $material) {
            $data[] = array(
                'value'=>$material['material_id'],
                'label'=>$material['material_name'],
                'attributes'=>array(
                    'data-material_id'=>$material['material_id'],
                    'data-material_name'=>$material['material_name'],
                    'data-office_id'=>$material['office_id'],
                    'data-office_name'=>$material['office_name'],
                    'data-department_id'=>$material['department_id'],
                    'data-department_name'=>$material['department_name'],
                )
            );
        }
        return $data;
    }

}