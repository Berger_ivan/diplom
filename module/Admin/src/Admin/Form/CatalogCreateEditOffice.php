<?php

namespace Admin\Form;

class CatalogCreateEditOffice extends \Zend\Form\Form
{

    public function __construct()
    {
        parent::__construct('catalog_create_edit_department');

        $this->setAttribute('method', 'post');

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Наименование');
        $element->setName('office_name');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Город');
        $element->setName('office_town');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Text();
        $element->setLabel('Адрес');
        $element->setName('office_adr');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $submit = new \Zend\Form\Element\Submit();
        $submit->setValue('Создать');
        $submit->setName('submit');
        $this->add($submit);

        $this->setInputFilter(new \Admin\Form\Filter\CreateEditOfficeFormFilter());
    }

}