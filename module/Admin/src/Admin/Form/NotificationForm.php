<?php

namespace Admin\Form;

use Application\Model\DepartmentModel;
use Application\Model\OfficeModel;
use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class NotificationForm extends Form
{

    public function __construct()
    {
        parent::__construct('request_form');

        $this->setAttribute('method','post');

        $element = new Select();
        $element->setLabel('Отделение');
        $element->setName('office_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getOffices());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Отдел');
        $element->setName('department_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getDepartments());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Text();
        $element->setLabel('Заголовок сообщения');
        $element->setName('notification_subject');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new Textarea();
        $element->setLabel('Текст сообщения');
        $element->setName('notification_text');
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new Submit();
        $element->setName('submit');
        $element->setValue('Создать');
        $this->add($element);

        $this->setInputFilter(new \Admin\Form\Filter\NotificationFormFilter());
    }

    protected function getOffices()
    {
        $model = new OfficeModel();
        return $model->getOfficesForSelect();
    }

    protected function getDepartments()
    {
        $model = new DepartmentModel();
        return $model->getDepartmentsForSelect();
    }


}