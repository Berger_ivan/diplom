<?php

namespace Admin\Form;

use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class CreateEditMaterialForm extends Form
{

    public function __construct()
    {
        parent::__construct('create_edit_material_form');

        $this->setAttribute('method','post');

        $element = new Text();
        $element->setLabel('Наименование');
        $element->setName('material_name');
        $element->setAttribute('id',$element->getName());
        $this->add($element);

        $element = new Select();
        $element->setLabel('Тип');
        $element->setName('mtype_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getMaterialTypes());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Text();
        $element->setLabel('Количество');
        $element->setName('material_count');
        $element->setAttribute('id',$element->getName());
        $element->setValue(1);
        $this->add($element);

        $element = new Select();
        $element->setLabel('Поставщик');
        $element->setName('provider_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getProviders());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Отделение');
        $element->setName('office_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getOffices());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Отдел');
        $element->setName('department_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getDepartments());
        $element->setEmptyOption('...');
        $this->add($element);

        $submit = new Submit();
        $submit->setName('submit');
        $this->add($submit);

        $this->setInputFilter(new \Admin\Form\Filter\CreateEditMaterialFormFilter());
    }

    protected function getProviders()
    {
        $m = new \Application\Model\ProviderModel();
        return $m->getProvidersForSelect();
    }

    protected function getOffices()
    {
        $m = new \Application\Model\OfficeModel();
        return $m->getOfficesForSelect();
    }

    protected function getDepartments()
    {
        $m = new \Application\Model\DepartmentModel();
        return $m->getDepartmentsForSelect();
    }

    protected function getMaterialTypes()
    {
        $m = new \Application\Model\MaterialModel();
        return $m->getMaterialTypes();
    }

}