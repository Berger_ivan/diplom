<?php

namespace Admin\Form;

use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class CreateEditUserForm extends Form
{

    public function __construct()
    {
        parent::__construct('create_edit_form');

        $this->setAttribute('method','post');

        $user_name = new Text();
        $user_name->setLabel('ФИО');
        $user_name->setName('user_name');
        $this->add($user_name);

        $user_dol = new Text();
        $user_dol->setLabel('Должность');
        $user_dol->setName('user_dol');
        $this->add($user_dol);

        $office_id = new Select();
        $office_id->setLabel('Отделение');
        $office_id->setName('office_id');
        $office_id->setValueOptions($this->getOffices());
        $office_id->setEmptyOption('...');
        $this->add($office_id);

        $department_id = new Select();
        $department_id->setLabel('Отдел');
        $department_id->setName('department_id');
        $department_id->setValueOptions($this->getDepartments());
        $department_id->setEmptyOption('...');
        $this->add($department_id);

        $user_mail = new Email();
        $user_mail->setLabel('E-mail');
        $user_mail->setName('user_mail');
        $this->add($user_mail);

        $user_mail = new Text();
        $user_mail->setLabel('Пароль');
        $user_mail->setName('user_pas');
        $this->add($user_mail);

        $submit = new Submit();
        $submit->setName('submit');
        $submit->setValue('Создать');
        $this->add($submit);

        $this->setInputFilter(new \Admin\Form\Filter\CreateEditUserFormFilter());
    }

    protected function getOffices()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT office.office_id'
        .', CONCAT(office.office_name," ",office.office_town," ",office.office_adr) as office_name'
        .' FROM office ORDER BY office.office_name';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['office_id']] = $res['office_name'];
        }
        return $selectData;
    }

    protected function getDepartments()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT department.department_id, department.department_name FROM department';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['department_id']] = $res['department_name'];
        }
        return $selectData;
    }

//    protected function getRequestTypes()
//    {
//        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
//        $sql = 'SELECT reqtype_id,reqtype_name FROM reqtype ORDER BY reqtype_name';
//        $statement = $dbAdapter->query($sql);
//        $result = $statement->execute();
//        $selectData = array();
//        foreach ($result as $res) {
//            $selectData[$res['reqtype_id']] = $res['reqtype_name'];
//        }
//        return $selectData;
//    }
//
//    protected function getMTypes()
//    {
//        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
//        $sql = 'SELECT mtype_id,mtype_name FROM mtype ORDER BY mtype_name';
//        $statement = $dbAdapter->query($sql);
//        $result = $statement->execute();
//        $selectData = array();
//        foreach ($result as $res) {
//            $selectData[$res['mtype_id']] = $res['mtype_name'];
//        }
//        return $selectData;
//    }

}