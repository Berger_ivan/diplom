<?php

namespace Admin\Form\Filter;


class CreateEditMtypeFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'mtype_name',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
    }
}