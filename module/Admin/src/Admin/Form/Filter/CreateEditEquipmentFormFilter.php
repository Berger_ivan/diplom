<?php

namespace Admin\Form\Filter;

use Zend\Validator\Identical;

class CreateEditEquipmentFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct($edit = false)
    {

        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();

        $validator = array();
        if (!$edit) {
            $validator = array(
                new \Zend\Validator\Db\NoRecordExists(
                    array(
                        'table' => 'equipment',
                        'field' => 'equipment_invnom',
                        'adapter' => $dbAdapter
                    )
                )
            );
        }

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_name',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_invnom',
            'required' => true,
            'filters' => array(),
            'validators' => $validator,
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'eqtype_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_characteristics',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_crtdate',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_warrdate',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_life',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'office_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'department_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_id',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'provider_id',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_cost',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));



        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_flagremote',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_flagrepair',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));


        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'repair_type',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'repair_startdate',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'repair_enddate',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'repair_provider_id',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'repair_reason',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));






    }
}