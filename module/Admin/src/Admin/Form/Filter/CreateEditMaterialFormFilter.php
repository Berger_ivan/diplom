<?php

namespace Admin\Form\Filter;

class CreateEditMaterialFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'material_name',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'mtype_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'material_count',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'provider_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'office_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'department_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

    }
}