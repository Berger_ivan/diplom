<?php

namespace Admin\Form\Filter;


class CreateEditDepartmentFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'department_name',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
    }
}