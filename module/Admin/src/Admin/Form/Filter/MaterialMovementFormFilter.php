<?php

namespace Admin\Form\Filter;

class MaterialMovementFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'old_office_id',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'old_department_id',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'material_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'mmove_count',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'office_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'department_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

    }
}