<?php

namespace Admin\Form\Filter;

use Zend\Validator\Identical;

class CreateEditUserFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_name',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_dol',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'office_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'department_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_mail',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'user_pas',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

    }
}