<?php

namespace Admin\Form\Filter;


class CreateEditProviderFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'provider_name',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'provider_town',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'provider_adr',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
    }
}