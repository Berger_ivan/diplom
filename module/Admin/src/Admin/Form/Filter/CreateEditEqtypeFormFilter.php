<?php

namespace Admin\Form\Filter;


class CreateEditEqtypeFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'eqtype_name',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
    }
}