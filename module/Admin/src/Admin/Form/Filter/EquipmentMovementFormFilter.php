<?php

namespace Admin\Form\Filter;

class EquipmentMovementFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'equipment_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'office_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'department_id',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'new_user_id',
            'required' => false,
            'filters' => array(),
            'validators' => array(
            ),
        ));

    }
}