<?php

namespace Admin\Form\Filter;


class CreateEditOfficeFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'office_name',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'office_town',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'office_adr',
            'required' => true,
            'filters' => array(),
            'validators' => array(
            ),
        ));
    }
}