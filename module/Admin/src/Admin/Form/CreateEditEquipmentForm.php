<?php

namespace Admin\Form;

use Application\Model\DepartmentModel;
use Application\Model\OfficeModel;
use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Date;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class CreateEditEquipmentForm extends Form
{

    public function __construct($edit = false)
    {
        parent::__construct('create_edit_equipment_form');

        $this->setAttribute('method','post');

        if ((bool)$edit === true) {
            $element = new Hidden();
            $element->setName('edit');
            $element->setValue(1);
            $this->add($element);
        }

        $element = new Text();
        $element->setLabel('Инвентарный номер');
        $element->setName('equipment_invnom');
        $element->setAttribute('id',$element->getName());
        $this->add($element);

        $element = new Text();
        $element->setLabel('Наименование');
        $element->setName('equipment_name');
        $element->setAttribute('id',$element->getName());
        $this->add($element);

        $element = new Select();
        $element->setLabel('Тип');
        $element->setName('eqtype_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getEqTypes());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Textarea();
        $element->setLabel('Характеристики');
        $element->setName('equipment_characteristics');
        $element->setAttribute('id',$element->getName());
        $this->add($element);

        $element = new Select();
        $element->setLabel('Поставщик');
        $element->setName('provider_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getProviders());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Отделение');
        $element->setName('office_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getOffices());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Отдел');
        $element->setName('department_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getDepartments());
        $element->setEmptyOption('...');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Пользователь');
        $element->setName('user_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getUsers());
        $element->setEmptyOption('...');
        $element->setUnselectedValue('-1');
        $this->add($element);

        $element = new Date();
        $element->setLabel('Дата поступления');
        $element->setName('equipment_crtdate');
        $element->setAttribute('id',$element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new Text();
        $element->setLabel('Дата окончания гарантийного периода');
        $element->setName('equipment_warrdate');
        $element->setAttribute('id',$element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new Text();
        $element->setLabel('Дата окончания срока службы');
        $element->setName('equipment_life');
        $element->setAttribute('id',$element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new Text();
        $element->setLabel('Стоимость оборудования');
        $element->setName('equipment_cost');
        $element->setAttribute('id',$element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Checkbox();
        $element->setLabel('Cписано');
        $element->setName('equipment_flagremote');
        $element->useHiddenElement();
        $element->setCheckedValue(1);
        $element->setUncheckedValue(0);
        $element->setAttribute('id', $element->getName());
        $this->add($element);

        $element = new \Zend\Form\Element\Checkbox();
        $element->setLabel('В ремонте');
        $element->setName('equipment_flagrepair');
        $element->useHiddenElement();
        $element->setCheckedValue(1);
        $element->setUncheckedValue(0);
        $element->setAttribute('id', $element->getName());
        $this->add($element);


        $element = new Select();
        $element->setLabel('Тип');
        $element->setName('repair_type');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions(array(
            '1'=>'ремонт',
            '2'=>'профилактика',
        ));
        $element->setValue('1');
        $this->add($element);

        $element = new Text();
        $element->setLabel('Дата начала ремонта');
        $element->setName('repair_startdate');
        $element->setAttribute('id',$element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new Text();
        $element->setLabel('Дата окончания ремонта');
        $element->setName('repair_enddate');
        $element->setAttribute('id',$element->getName());
//        $element->setOptions(array('format' => 'd.m.Y'));
        $element->setAttribute('data-need-datepicker', 'true');
        $this->add($element);

        $element = new Select();
        $element->setLabel('Фирама, выполняющая ремонт');
        $element->setName('repair_provider_id');
        $element->setAttribute('id',$element->getName());
        $element->setValueOptions($this->getProviders());
        $element->setValue('1');
        $this->add($element);

        $element = new Textarea();
        $element->setLabel('Причина ремонта');
        $element->setName('repair_reason');
        $element->setAttribute('id',$element->getName());
        $this->add($element);


        $submit = new Submit();
        $submit->setName('submit');
        $this->add($submit);

        $this->setInputFilter(new \Admin\Form\Filter\CreateEditEquipmentFormFilter($edit));
    }

    protected function getOffices()
    {
        $model = new OfficeModel();
        return $model->getOfficesForSelect();
    }

    protected function getUsers()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT `user`.`user_id` as id, `user`.`user_name` as name FROM `user`';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['id']] = $res['name'];
        }
        return $selectData;
    }

    protected function getEqTypes()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT `eqtype`.`eqtype_id` as id, `eqtype`.`eqtype_name` as name FROM `eqtype`';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['id']] = $res['name'];
        }
        return $selectData;
    }

    protected function getProviders()
    {
        $dbAdapter = \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
        $sql = 'SELECT `provider`.`provider_id` as id, `provider`.`provider_name` as name FROM `provider`';
        $statement = $dbAdapter->query($sql);
        $result = $statement->execute();
        $selectData = array();
        foreach ($result as $res) {
            $selectData[$res['id']] = $res['name'];
        }
        return $selectData;
    }

    protected function getDepartments()
    {
        $model = new DepartmentModel();
        return $model->getDepartmentsForSelect();
    }

}