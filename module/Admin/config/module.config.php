<?php

return array(
    'router' => array(
        'routes' => array(
//            'admin' => array(
//                'type'    => 'Zend\Mvc\Router\Http\Literal',
//                'options' => array(
//                    'route'    => '/admin',
//                    'defaults' => array(
//                        '__NAMESPACE__' => 'Admin\Controller',
//                        'controller'    => 'Index',
//                        'action'        => 'index',
//                    ),
//                ),
//                'may_terminate' => true,
//                'child_routes' => array(
//                    'default' => array(
//                        'type'    => 'Segment',
//                        'options' => array(
//                            'route'    => '/[:controller[/:action]]',
//                            'constraints' => array(
//                                'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
//                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
//                            ),
//                            'defaults' => array(
//                                '__NAMESPACE__' => 'Admin\Controller',
//                                'controller'    => 'Index',
//                                'action'    => 'index',
//                            ),
//                        ),
//                    ),
//                ),
//            ),
            'admin' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin[/:controller[/:action]]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                    'constraints' => array(
                        'controller'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'ru_RU',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\AdminAbstract' => 'Admin\Controller\AdminAbstractController',
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\Request' => 'Admin\Controller\RequestController',
            'Admin\Controller\User' => 'Admin\Controller\UserController',
            'Admin\Controller\Equipment' => 'Admin\Controller\EquipmentController',
            'Admin\Controller\Material' => 'Admin\Controller\MaterialController',
            'Admin\Controller\Notification' => 'Admin\Controller\NotificationController',
            'Admin\Controller\Catalog' => 'Admin\Controller\CatalogController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'admin/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'admin404Layout'           => __DIR__ . '/../view/layout/admin404Layout.phtml',
            'admin/index/index' => __DIR__ . '/../view/admin/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
