<?php
return array(
    'acl' => array(
        'roles' => array(
            'guest'   => null,
            'customer'  => 'guest',
            'manager'  => 'guest',
            'admin'  => null,
        ),
        'resources' => array(
            'allow' => array(
                'user' => array(
                    'login' => 'guest',
                    'all'   => 'member'
                )
            )
        )
    )
);