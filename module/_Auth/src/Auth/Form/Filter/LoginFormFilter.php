<?php

namespace Auth\Form\Filter;

class LoginFormFilter extends \Zend\InputFilter\InputFilter
{

    public function __construct()
    {

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'login',
            'required' => true,
            'filters' => array(
                new \Zend\Filter\StripTags(),
                new \Zend\Filter\StringTrim(),
            ),
            'validators' => array(
                new \Zend\Validator\NotEmpty(),
            ),
        ));

        $this->add(array(
            'type'=>'Zend\InputFilter\Input',
            'name'=>'password',
            'required' => true,
            'filters' => array(),
            'validators' => array(
                new \Zend\Validator\NotEmpty(),
            ),
        ));

    }
}