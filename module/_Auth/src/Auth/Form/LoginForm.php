<?php

namespace Auth\Form;

use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;
use Zend\Form\Form;

class LoginForm extends Form
{

    public function __construct()
    {
        parent::__construct('login_form');


        $this->setAttribute('method','post');

        $login = new Text();
        $login->setLabel('Login');
        $login->setName('login');
        $this->add($login);

        $password = new Password();
        $password->setLabel('Password');
        $password->setName('password');
        $this->add($password);

        $remember = new Checkbox();
        $remember->setLabel('Remember me');
        $remember->setName('remember');
        $this->add($remember);

        $submit = new Submit();
        $submit->setName('submit');
        $submit->setValue('Submit');
        $this->add($submit);

        $this->setInputFilter(new \Auth\Form\Filter\LoginFormFilter());
    }
}