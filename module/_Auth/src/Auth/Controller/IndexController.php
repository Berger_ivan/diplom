<?php

namespace Auth\Controller;

use Auth\Form\LoginForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    protected $_storage;
    protected $_authService;

    public function init()
    {
        $storage = $this->getServiceLocator()->get('Auth\Model\AuthService')->getStorage()->read();
        $storage = $this->getServiceLocator()->get('Auth\Model\AuthStorage')->read();
    }

    protected function getAuthService()
    {
        if (!$this->_authService) {
            $this->_authService = $this->getServiceLocator()->get('Auth\Model\AuthService');
        }
        return $this->_authService;
    }
    protected function getSessionStorage()
    {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()->get('Auth\Model\AuthStorage');
        }
        return $this->_storage;
    }

    public function loginAction()
    {
        $storage = $this->getServiceLocator()->get('Auth\Model\AuthService')->getStorage()->read();
        $storage = $this->getServiceLocator()->get('Auth\Model\AuthStorage')->read();

        if ($this->getAuthService()->hasIdentity()) {
            return $this->redirect()->toRoute('home');
        }

        $viewModel = new ViewModel();
        $form = new LoginForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->getAuthService()->getAdapter()
                    ->setIdentity($form->get('login')->getValue())
                    ->setCredential($form->get('password')->getValue());

                $result = $this->getAuthService()->authenticate();
                foreach ($result->getMessages() as $message) {
                    $this->flashmessenger()->addMessage($message);
                }
                if ($result->isValid()) {
                    if ($request->getPost('rememberme') == 1 ) {
                        $this->getSessionStorage()->setRememberMe(1);
                        $this->getAuthService()->setStorage($this->getSessionStorage());
                    }
                    $this->getAuthService()->getStorage()->write($request->getPost('login'));
                    return $this->redirect()->toRoute('home');
                }
            }
        }
        $viewModel->setVariable('form', $form);
        $viewModel->setVariable('messages', $this->flashmessenger()->getMessages());
        return $viewModel;
    }

    public function logoutAction()
    {
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        $this->flashmessenger()->addMessage("You've been logged out");
        return $this->redirect()->toRoute('login');
    }

}