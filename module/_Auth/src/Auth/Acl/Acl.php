<?php

namespace Admin\Acl;

class Acl extends \Zend\Permissions\Acl\Acl
{

    const DEFAULT_ROLE = 'guest';

    public function __construct($config)
    {
        if (!isset($config['acl']['roles']) || !isset($config['acl']['resources'])) {
            throw new \Exception('Invalid ACL Config found');
        }
        $roles = $config['acl']['roles'];
        if (!isset($roles[self::DEFAULT_ROLE])) {
            $roles[self::DEFAULT_ROLE] = '';
        }
        $this->_addRoles($roles)
             ->_addResources($config['acl']['resources']);
    }

    protected function _addRoles($roles)
    {
        foreach ($roles as $name => $parent) {
            if (!$this->hasRole($name)) {
                $parents = null;
                if (!empty($parent)) {
                    $parents = explode(',', $parent);
                }
                $this->addRole(new \Zend\Permissions\Acl\Role\GenericRole($name), $parents);
            }
        }
        return $this;
    }

    protected function _addResources($resources)
    {
        foreach ($resources as $permission => $controllers) {
            foreach ($controllers as $controller => $actions) {
                if ($controller == 'all') {
                    $controller = null;
                } else {
                    if (!$this->hasResource($controller)) {
                        $this->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($controller));
                    }
                }
                foreach ($actions as $action => $role) {
                    if ($action == 'all') {
                        $action = null;
                    }
                    if ($permission == 'allow') {
                        $this->allow($role, $controller, $action);
                    } elseif ($permission == 'deny') {
                        $this->deny($role, $controller, $action);
                    } else {
                        throw new \Exception('No valid permission defined: ' . $permission);
                    }
                }
            }
        }
        return $this;
    }

}