<?php

namespace Auth;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $e->getApplication()->getServiceManager()->get('Zend\Db\Adapter\Adapter');

        $e->getApplication()->getEventManager()->attach(MvcEvent::EVENT_DISPATCH, function ($e) {
            $controller = $e->getTarget();
            $c = get_class($controller);
            if (__NAMESPACE__ === substr($c, 0, strpos($c, "\\"))) {
                if (FALSE === $e->getViewModel()->terminate()) {
                    $controller->layout(strtolower(__NAMESPACE__)."/layout");
                }
            }
        });
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories'=>array(
                'Auth\Model\AuthStorage'=>function($serviceManager){
                    return new \Auth\Model\AuthStorage('diplom');
                },
                'Auth\Model\AuthService'=>function($serviceManager){
                    $dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new \Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter(
                        $dbAdapter, 'users', 'login', 'password', '?'
                    );
                    $authService = new \Zend\Authentication\AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($serviceManager->get('Auth\Model\AuthStorage'));
                    return $authService;
                },
            ),
        );
    }

}