<?php

return array(
    'db' => array(
        'driver'=>'Pdo',
        'dsn'=>'mysql:dbname=accequip;host=127.0.0.1',
        'driver_options'=>array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
        'username'=>'root',
        'password'=>'root',
    ),
    'service_manager'=>array(
        'factories'=>array(
            'Zend\Db\Adapter\Adapter' => function ($serviceManager) {
                $adapterFactory = new Zend\Db\Adapter\AdapterServiceFactory();
                $adapter = $adapterFactory->createService($serviceManager);
                \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::setStaticAdapter($adapter);
                return $adapter;
            },
        ),
    ),
    'email'=>'sergeikos88@gmail.com',
);
